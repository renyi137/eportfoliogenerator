/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.LanguagePropertyType;
import static epg.LanguagePropertyType.LABEL_SAVE_UNSAVED_WORK;
import static epg.StartupConstants.*;
import epg.error.ErrorHandler;
import epg.file.EPortfolioExporter;
import epg.file.EPortfolioFileLoadManager;
import epg.file.EPortfolioFileManager;
import epg.model.Site;
import epg.view.EPortfolioGeneratorView;
import epg.view.YesNoCancelDialog;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class FileController {
     // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private EPortfolioGeneratorView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private EPortfolioFileManager EPortfolioIO;
    private EPortfolioFileLoadManager EPortfolioIO2;
    // THIS ONE EXPORTS OUR SITE 
    private EPortfolioExporter siteExporter;

    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
    public FileController(EPortfolioGeneratorView initUI, 
	    EPortfolioFileManager initEPortfolioIO, EPortfolioFileLoadManager initEPortfolioIO2,
	    EPortfolioExporter initSiteExporter) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        EPortfolioIO = initEPortfolioIO;
        EPortfolioIO2 = initEPortfolioIO2;
	siteExporter = initSiteExporter;
    }
    
    public void markAsEdited() {
        saved = false;
        ui.updateFileToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewRequest() {
        boolean continueToMakeNew = true;
        if (!saved) {
            try {
                String s=promptToSave();// THE USER CAN OPT OUT HERE WITH A CANCEL
                if(s.equals("Cancel")){
                    continueToMakeNew=false;
                }
                
            } catch (IOException ex) {
                Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (continueToMakeNew) {
            // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
            Site site = ui.getSite();
            site.reset();
            saved = true;
            
            // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateFileToolbarControls(saved);
            
            // MAKE SURE THE TITLE CONTROLS ARE ENABLED
            
            ui.reloadPageSelectPane();
           
        }
    }
    public boolean handleSaveRequest() {
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    Site site = ui.getSite();
	    
            // SAVE IT TO A FILE
            if(!site.isIsSaved()){
                String s=promptToSave();
                
                if(s.equals("Yes")){
                   saved = true;
                }
            }
            else{
                EPortfolioIO.saveEPortfolio(site,site.getTitle());
                saved=true;
                
            }
            // MARK IT AS SAVED
            

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateFileToolbarControls(saved);
	    return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
	    return false;
        }
    }
    public boolean handleSaveAsRequest(){
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    Site site = ui.getSite();
	    
            // SAVE IT TO A FILE
            

            String s=promptToSave();
                
                if(s.equals("Yes")){
                   saved = true;
                }

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateFileToolbarControls(saved);
	    return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
	    return false;
        }
    }
    public void handleLoadRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                String s=promptToSave();// THE USER CAN OPT OUT HERE WITH A CANCEL
                if(s.equals("Cancel")){
                    continueToOpen=false;
                }
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING);
        }
    }
    public void handleExportRequest(String name){
        try {
	    
	    Site site = ui.getSite();
            EPortfolioIO.saveEPortfolio(site, "test");
	    siteExporter.exportSite(site,name);
	
	    // THEN VIEW THE SITE
	   
	}
	catch(Exception e) {
	    e.printStackTrace();
	}
    }
    
    public String promptToSave() throws IOException{
        YesNoCancelDialog yesNoCancelDialog = new YesNoCancelDialog(ui.getWindow());
	PropertiesManager props = PropertiesManager.getPropertiesManager();
        yesNoCancelDialog.show(props.getProperty(LABEL_SAVE_UNSAVED_WORK));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();	
	boolean saveWork = selection.equals(YesNoCancelDialog.YES);
        
        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            Site site = ui.getSite();
            EPortfolioIO.saveEPortfolio(site, yesNoCancelDialog.getFileName());
            return selection;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else {
            return selection;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        
    }
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_EPORTFOLIO));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		Site site = ui.getSite();
                EPortfolioIO2.loadSite(site, selectedFile.getAbsolutePath());
                ui.reloadPageSelectPane();
                saved = true;
                ui.updateFileToolbarControls(saved);
            } catch (Exception e) {
                 Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, e);
                //ErrorHandler eH = ui.getErrorHandler();
		//eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
            }
        }
    }
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
}


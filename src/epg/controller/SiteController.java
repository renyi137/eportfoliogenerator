/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import static epg.StartupConstants.*;
import epg.model.Site;
import epg.view.EPortfolioGeneratorView;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class SiteController {
    private EPortfolioGeneratorView ui;
    public SiteController(EPortfolioGeneratorView initUi){
        ui=initUi;
}
    public void processAddPageRequest() {
	Site site = ui.getSite();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	site.addPage(DEFAULT_TILTLE,
    DEFAULT_STUDENT_NAME,
   DEFAULT_FOOTER,
    DEFAULT_LAYOUT,
   DEFAULT_COLOR_TEMPLATE,PATH_IMAGES,DEFAULT_BANNER_IMAGE,false,false,false,FONTS_FAMILY_1,FONTS_SIZE_1);
        
	ui.updateFileToolbarControls(false);
        ui.reloadPageSelectPane();
    }
    public void processRemovePageRequest(){
        Site site=ui.getSite();
        site.removeSelectedPage();
        ui.updateFileToolbarControls(false);
        ui.reloadPageSelectPane();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.LanguagePropertyType;
import static epg.StartupConstants.PATH_IMAGES;
import epg.error.ErrorHandler;
import epg.model.Component;
import epg.model.HeaderComponent;
import epg.model.ImageComponent;
import epg.model.ListComponent;
import epg.model.ParagraphComponent;
import epg.model.SlideShowModel;
import epg.model.VideoComponent;
import epg.view.HeaderComponentEditorDialog;
import epg.view.HyperlinkAddDialog;
import epg.view.HyperlinkEditorDialog;
import epg.view.ImageComponentEditorDialog;
import epg.view.ListComponentEditorDialog;
import epg.view.PageEditorView;
import epg.view.ParagraphComponentEditorDialog;
import epg.view.SlideshowComponentEditorDialog;
import epg.view.VideoComponentEditorDialog;
import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 *
 * @author yiren
 */
public class PageEditorController {
    PageEditorView pageEditor;
    public PageEditorController(PageEditorView initpageEditor){
        pageEditor=initpageEditor;
    }    
    public void handleAddHeaderRequest(boolean isNewComponent){
        HeaderComponentEditorDialog headerComponentEditorDialog=new  HeaderComponentEditorDialog(pageEditor, isNewComponent);
        headerComponentEditorDialog.showAndWait();
    }
    public void handleAddListRequest(boolean isNewComponent){
        ListComponentEditorDialog listComponentEditorDialog=new  ListComponentEditorDialog(pageEditor, isNewComponent);
        listComponentEditorDialog.showAndWait();
    }
    public void handleAddParagraphRequest(boolean isNewComponent){
        ParagraphComponentEditorDialog paragraphComponentEditorDialog=new  ParagraphComponentEditorDialog(pageEditor, isNewComponent);
        paragraphComponentEditorDialog.showAndWait();
    }
    public void handleAddImageRequest(boolean isNewComponent){
        ImageComponentEditorDialog imageComponentEditorDialog=new  ImageComponentEditorDialog(pageEditor, isNewComponent);
        imageComponentEditorDialog.showAndWait();
    }
    public void handleAddVideoRequest(boolean isNewComponent){
        VideoComponentEditorDialog videoComponentEditorDialog=new  VideoComponentEditorDialog(pageEditor, isNewComponent);
        videoComponentEditorDialog.showAndWait();
    }
    public void handleAddSlideshowRequest(boolean isNewComponent){
        SlideshowComponentEditorDialog slideshowComponentEditorDialog=new  SlideshowComponentEditorDialog(pageEditor, isNewComponent);
        slideshowComponentEditorDialog.showAndWait();
    }
    public void handleAddHyperlinkRequest(boolean isNewComponent){
        HyperlinkEditorDialog hyperlinkshowComponentEditorDialog=new  HyperlinkEditorDialog(pageEditor, isNewComponent);
        hyperlinkshowComponentEditorDialog.showAndWait();
    }
    public void handleEditRequest(){
        
        Component component=pageEditor.getPage().getSelectedComponent();
        if(component instanceof HeaderComponent){
            handleAddHeaderRequest(false);
        }
        if(component instanceof ListComponent){
            handleAddListRequest(false);
        }
        if(component instanceof ParagraphComponent){
            handleAddParagraphRequest(false);
        }
        if(component instanceof ImageComponent){
           handleAddImageRequest(false);
        }
        if(component instanceof VideoComponent){
           handleAddVideoRequest(false);
        }
        if(component instanceof SlideShowModel){
           handleAddSlideshowRequest(false);
        }
        
    }
    public void handleRemoveRequest(){
        pageEditor.getPage().removeSelectedComponent();
        pageEditor.reloadComponents();
    }
    public void handleChangeBannerImageRequest(){
        String path="";
        String fileName="";
        
        FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    fileName = file.getName();
            pageEditor.getPage().setBannerImageFileName(fileName);
            pageEditor.getPage().setBannerImagePath(path);
            pageEditor.reloadBannerImage();
        }
        
    }
   public void handleAddHyperlinkRequest(){
       HyperlinkAddDialog hyperlinkAddDialog=new  HyperlinkAddDialog(pageEditor);
       hyperlinkAddDialog.showAndWait();
   }
    
}

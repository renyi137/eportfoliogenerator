/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.view.EPortfolioGeneratorView;

/**
 *
 * @author yiren
 */
public class ModeController {
    EPortfolioGeneratorView ui;
    public ModeController(EPortfolioGeneratorView initUi){
        ui=initUi;
}
    public void handleEditModeRequest(){
        ui.reloadPageEditorPane();
    }
    public void handleViewerModeRequest(){
        ui.reloadSiteViewerPane();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author yiren
 */
public class Hyperlink {
    String source;
    String toLink;
    int location;
    public Hyperlink(String initSource, String initToLink, int initLocation){
        source=initSource;
        toLink=initToLink;
        location=initLocation;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getToLink() {
        return toLink;
    }

    public void setToLink(String toLink) {
        this.toLink = toLink;
    }
    
}

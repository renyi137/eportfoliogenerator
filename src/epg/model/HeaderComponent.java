/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author yiren
 */
public class HeaderComponent extends Component{
    String headerText;
    public HeaderComponent(String initHeaderText){
        headerText=initHeaderText;
    }
    public void setContent(String headerText){
        this.headerText=headerText;
    }
    public String getContent(){
        return headerText;
    }
}

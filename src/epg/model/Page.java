/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author yiren
 */
public class Page {
    private String title;
    private String studentName;
    private String footer;
    private String layout;
    private String colorTemplate;
    String BannerImageFileName;
    String BannerImagePath;
    ObservableList<Component> components;
    Component selectedComponent;
    String fontFamily;
    String fontSize;
    boolean isBold;
    boolean isItalic;
    boolean isUnderlined;
    ObservableList<String> orderOfComponents;
    public Page(String initTitle, String initStudentName, String initFooter, String initLayout,
            String initColorTemplate, String initBannerImagePath, String initBannerImageFileName,boolean initIsBold,
            boolean initIsItalic,boolean initIsUnderlined, String initFontFamily, String initFontSize){
        title=initTitle;
        studentName=initStudentName;
        footer=initFooter;
        layout=initLayout;
        colorTemplate=initColorTemplate;
        BannerImageFileName=initBannerImageFileName;
        BannerImagePath=initBannerImagePath;
        components = FXCollections.observableArrayList();
        isBold=initIsBold;
        isItalic=initIsItalic;
        isUnderlined=initIsUnderlined;
        fontFamily=initFontFamily;
        fontSize=initFontSize;
        orderOfComponents=FXCollections.observableArrayList();
        
    }
    public String getTitle()
    {
        return title;
    }
    public void addHeaderComponent(String headerText){
        components.add(new HeaderComponent(headerText));
        
    }
    public void addComponent(Component componentToAdd){
        components.add(componentToAdd);
    }
    public void addListComponent(ListComponent listComponentToAdd){
        components.add(listComponentToAdd);
    }
    public Component getSelectedComponent(){
        return selectedComponent;
    }
    public ObservableList<Component> getComponents(){
        return components;
    }
    public boolean isSelectedComponent(Component component){
        return component==selectedComponent;
    }
    public void setSelectedComponent(Component component){
        selectedComponent=component;
    }
    public String getBannerImagePath(){
        return BannerImagePath;
    }
    public String getBannerImageFileName(){
        return BannerImageFileName;
    }
    public String getLayout(){
        return layout;
    }
    public String getColorTemplate(){
        return colorTemplate;
    }
    public void removeSelectedComponent(){
        components.remove(selectedComponent);
        selectedComponent=null;
    }
    public void setTitle(String newTitle){
        title=newTitle;
    }
    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    public boolean isIsBold() {
        return isBold;
    }

    public void setIsBold(boolean isBold) {
        this.isBold = isBold;
    }

    public boolean isIsItalic() {
        return isItalic;
    }

    public void setIsItalic(boolean isItalic) {
        this.isItalic = isItalic;
    }

    public boolean isIsUnderlined() {
        return isUnderlined;
    }

    public void setIsUnderlined(boolean isUnderlined) {
        this.isUnderlined = isUnderlined;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public void setColorTemplate(String colorTemplate) {
        this.colorTemplate = colorTemplate;
    }

    public void setBannerImageFileName(String BannerImageFileName) {
        this.BannerImageFileName = BannerImageFileName;
    }

    public void setBannerImagePath(String BannerImagePath) {
        this.BannerImagePath = BannerImagePath;
    }

    public void setComponents(ObservableList<Component> components) {
        this.components = components;
    }
    public void makeOrderOfComponents(){
            orderOfComponents.clear();
        
        for(Component component: components){
            if(component instanceof HeaderComponent){
                orderOfComponents.add("h");
            }
            if(component instanceof ListComponent){
                orderOfComponents.add("l");
            }
            if(component instanceof ParagraphComponent){
                orderOfComponents.add("p");
            }
            if(component instanceof ImageComponent){
                orderOfComponents.add("i");
            }
            if(component instanceof VideoComponent){
                orderOfComponents.add("v");
            }
            if(component instanceof SlideShowModel){
                orderOfComponents.add("s");
            }
        }
    }

    public ObservableList<String> getOrderOfComponents() {
        return orderOfComponents;
    }

    public void setOrderOfComponents(ObservableList<String> orderOfComponents) {
        this.orderOfComponents = orderOfComponents;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.ObservableList;

/**
 *
 * @author yiren
 */
public class ParagraphComponent extends Component{
    String content;
    String fontFamily;
    String fontSize;
    boolean isBold;
    boolean isItalic;
    boolean isUnderlined;
    ObservableList<Hyperlink> hyperlinks;
    public ParagraphComponent(){}

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    public boolean isIsBold() {
        return isBold;
    }

    public void setIsBold(boolean isBold) {
        this.isBold = isBold;
    }

    public boolean isIsItalic() {
        return isItalic;
    }

    public void setIsItalic(boolean isItalic) {
        this.isItalic = isItalic;
    }

    public boolean isIsUnderlined() {
        return isUnderlined;
    }

    public void setIsUnderlined(boolean isUnderlined) {
        this.isUnderlined = isUnderlined;
    }
    public void addHyperlink(String initSource, String initToLink, int initLocation){
        hyperlinks.add(new Hyperlink(initSource, initToLink, initLocation));
    }

    public ObservableList<Hyperlink> getHyperlinks() {
        return hyperlinks;
    }

    public void setHyperlinks(ObservableList<Hyperlink> hyperlinks) {
        this.hyperlinks = hyperlinks;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import epg.LanguagePropertyType;
import epg.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class Site {
    ObservableList<Page> pages;
    EPortfolioGeneratorView ui;
    Page selectedPage;
    private String title;
    private boolean isSaved;
    public Site(EPortfolioGeneratorView initUi){
        ui=initUi;
        isSaved=false;
	pages = FXCollections.observableArrayList();
	reset();
    }
    public void setSelectedPage(Page initSelectedPage) {
	selectedPage = initSelectedPage;
    }
    public void reset() {
        pages.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selectedPage = null;
    }
    
    public ObservableList<Page> getPages(){
        return pages;
    }
    public boolean isSelectedPage(Page testPage){
        return selectedPage == testPage;
    }
    public void addPage(String initTitle, String initStudentName, String initFooter, String initLayout,
            String initColorTemplate, String initBannerImageFileName, String initBannerImagePath, boolean initIsBold,
            boolean initIsItalic,boolean initIsUnderlined, String initFontFamily, String initFontSize){
        Page pageToAdd = new Page(initTitle, initStudentName, initFooter, initLayout, 
            initColorTemplate, initBannerImageFileName, initBannerImagePath, initIsBold,
             initIsItalic, initIsUnderlined,initFontFamily,initFontSize);
	pages.add(pageToAdd);
	
    }
    public Page getSelectedPage(){
        return selectedPage;
    }
    public void removeSelectedPage(){
        pages.remove(selectedPage);
        selectedPage=null;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public EPortfolioGeneratorView getUi() {
        return ui;
    }

    public void setUi(EPortfolioGeneratorView ui) {
        this.ui = ui;
    }

    public boolean isIsSaved() {
        return isSaved;
    }

    public void setIsSaved(boolean isSaved) {
        this.isSaved = isSaved;
    }
    
    
}

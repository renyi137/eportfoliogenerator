/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 *
 * @author yiren
 */
public class ListComponent extends Component{
    ObservableList<Item> list;
    String fontFamily;
    String fontSize;
    boolean isBold;
    boolean isItalic;
    boolean isUnderlined;
    
    public ListComponent(){
        list=FXCollections.observableArrayList();
        isBold=false;
        isItalic=false;
        isUnderlined=false;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public String getFontSize() {
        return fontSize;
    }

    public boolean isIsBold() {
        return isBold;
    }

    public boolean isIsItalic() {
        return isItalic;
    }

    public boolean isIsUnderlined() {
        return isUnderlined;
    }
    public ObservableList<Item> getList(){
        return list;
    }
    public void addItem(String itemText){
        list.add(new Item(itemText));
    }
     public void removeItem(){
        list.remove(list.size()-1);
    }
     public void setFontFamily(String initFontFamily){
         fontFamily=initFontFamily;
     }
     public void setFontSize(String initFontSize){
         fontSize=initFontSize;
     }
     public void setIsBold(boolean is){
         isBold=is;
     }
     public void setIsItalic(boolean is){
         isItalic=is;
     }
     public void setIsUnderlined(boolean is){
         isUnderlined=is;
     }
     public boolean isEmpty(){
         return list.isEmpty();
     }
     public void setListItems(VBox listVBox){
         for(int i=0;i<listVBox.getChildren().size();i++){
             addItem(((TextField)listVBox.getChildren().get(i)).getText());
         }
         
     }

    public void setList(ObservableList<Item> list) {
        this.list = list;
    }
    

    
             
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.ObservableList;

/**
 *
 * @author yiren
 */
public class Item {
    String content;
    ObservableList<Hyperlink> hyperlinks;
    public Item(String initContent){
        content=initContent;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public void addHyperlink(String initSource, String initToLink, int initLocation){
        hyperlinks.add(new Hyperlink(initSource, initToLink, initLocation));
    }

    public ObservableList<Hyperlink> getHyperlinks() {
        return hyperlinks;
    }

    public void setHyperlinks(ObservableList<Hyperlink> hyperlinks) {
        this.hyperlinks = hyperlinks;
    }
    
}

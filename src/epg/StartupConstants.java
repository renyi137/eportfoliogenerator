/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg;

/**
 *
 * @author yiren
 */
public class StartupConstants {

    public static String ENGLISH_LANG = "English";
    public static String CHINESE_LANG = "Finnish";
    
    public static String FONTS_FAMILY_1="Montserrat";
    public static String FONTS_FAMILY_2="Open Sans Condensed";
    public static String FONTS_FAMILY_3="Poiret One";
    public static String FONTS_FAMILY_4="Titillium Web";
    public static String FONTS_FAMILY_5="Dosis";
    public static String FONTS_SIZE_1="8pt";
    public static String FONTS_SIZE_2="16pt";
    public static String FONTS_SIZE_3="24pt";
    public static String FONTS_SIZE_4="32pt";
    public static String FONTS_SIZE_5="40pt";
    public static String LAYOUT_1="layout1";
    public static String LAYOUT_2="layout2";
    public static String LAYOUT_3="layout3";
    public static String LAYOUT_4="layout4";
    public static String LAYOUT_5="layout5";
    public static String COLOR_TEMPLATE_1="color1";
    public static String COLOR_TEMPLATE_2="color2";
    public static String COLOR_TEMPLATE_3="color3";
    public static String COLOR_TEMPLATE_4="color4";
    public static String COLOR_TEMPLATE_5="color5";
    
    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String UI_PROPERTIES_FILE_NAME_English = "properties_EN.xml";
    public static String UI_PROPERTIES_FILE_NAME_Finnish = "properties_CN.xml";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_EPORTFOLIO = PATH_DATA + "EPortfolio/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_CSS = "/epg/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "EPortfolioGeneratorStyle.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "EPG_Logo.png";
    public static String ICON_NEW = "New.png";
    public static String ICON_LOAD = "Load.png";
    public static String ICON_SAVE = "Save.png";
    public static String ICON_SAVEAS = "SaveAs.png";
    public static String ICON_EXPORT= "Export.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_PAGE = "Add.png";
    public static String ICON_REMOVE_PAGE = "Remove.png";
    public static String ICON_EDIT = "Edit.png";
    public static String ICON_VIEW = "View.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_PREVIOUS = "Previous.png";
    public static String ICON_NEXT = "Next.png";
    public static String ICON_LAYOUT_1="Layout1.png";
    public static String ICON_LAYOUT_2="Layout2.png";
    public static String ICON_LAYOUT_3="Layout3.png";
    public static String ICON_LAYOUT_4="Layout4.png";
    public static String ICON_LAYOUT_5="Layout5.png";
    public static String ICON_COLOR_1="Color1.png";
    public static String ICON_COLOR_2="Color2.png";
    public static String ICON_COLOR_3="Color3.png";
    public static String ICON_COLOR_4="Color4.png";
    public static String ICON_COLOR_5="Color5.png";
    public static String ICON_BANNER_IMAGE="BannerImage.png";
    public static String ICON_BOLD="Bold.png";
    public static String ICON_ITALIC="Italic.png";
    public static String ICON_UNDERLINED="Underlined.png";
    public static String ICON_ADD_HEADER="AddHeader.png";
    public static String ICON_ADD_LIST="AddList.png";
    public static String ICON_ADD_PARAGRAPH="AddParagraph.png";
    public static String ICON_ADD_IMAGE="AddImage.png";
    public static String ICON_ADD_SLIDESHOW="AddSlideshow.png";
    public static String ICON_ADD_VIDEO="AddVideo.png";
    public static String ICON_REMOVE_COMPONENT="RemoveComponent.png";
    public static String ICON_EDIT_COMPONENT="EditComponent.png";
    public static String ICON_ADD_HYPERLINK="AddHyperlink.png";
    public static String ICON_REMOVE_ITEM="RemoveItem.png";
    public static String ICON_ADD_ITEM="AddItem.png";
    public static String ICON_ADD_SLIDE = "Add.png";
    public static String ICON_REMOVE_SLIDE = "Remove.png";
    
    
    
    // UI SETTINGS
    public static String    DEFAULT_TILTLE="newpage";
    public static String    DEFAULT_STUDENT_NAME="ENTER A STUDEN NAME";
    public static String    DEFAULT_FOOTER="ENTER A FOOTER";
    public static String    DEFAULT_LAYOUT="layout1";
    public static String    DEFAULT_COLOR_TEMPLATE="color1";
    public static String    DEFAULT_BANNER_IMAGE = "DefaultBannerImage.png";
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_SLIDE_SHOW_HEIGHT = 500;
    
    
    // CSS STYLE SHEET CLASSES
    
    // CSS - FOR THE LANGUAGE SELECTION DIALOG
    public static String    CSS_CLASS_LANG_DIALOG_PANE = "lang_dialog_pane";
    public static String    CSS_CLASS_LANG_PROMPT = "lang_prompt";
    public static String    CSS_CLASS_LANG_COMBO_BOX = "lang_combo_box";
    public static String    CSS_CLASS_LANG_OK_BUTTON = "lang_ok_button";

    // CSS - FOR THE TOOLBARS
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_PANE = "horizontal_toolbar_pane";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_PANE = "vertical_toolbar_pane";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";

    // CSS - SLIDESHOW EDITING
    public static String    CSS_CLASS_TITLE_PANE = "title_pane";
    public static String    CSS_CLASS_TITLE_PROMPT = "title_prompt";
    public static String    CSS_CLASS_TITLE_TEXT_FIELD = "title_text_field";
    public static String    CSS_CLASS_CAPTION_PROMPT = "caption_prompt";
    public static String    CSS_CLASS_CAPTION_TEXT_FIELD = "caption_text_field";
    public static String    CSS_CLASS_PAGE_COLOR_1="page_color_1";
    public static String    CSS_CLASS_PAGE_COLOR_2="page_color_2";
    public static String    CSS_CLASS_PAGE_COLOR_3="page_color_3";
    public static String    CSS_CLASS_PAGE_COLOR_4="page_color_4";
    public static String    CSS_CLASS_PAGE_COLOR_5="page_color_5";
    public static String    CSS_CLASS_BUTTON_SELECTED="button_selected";
    // CSS - PANELS
    public static String    CSS_CLASS_WORKSPACE = "workspace";
    public static String    CSS_CLASS_SLIDES_EDITOR_PANE = "slides_editor_pane";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW = "selected_slide_edit_view";
    public static String    CSS_CLASS_COMPONENT_EDIT_VIEW = "component_edit_view";
    public static String    CSS_CLASS_SELECTED_COMPONENT_EDIT_VIEW = "selected_component_edit_view";

    // UI LABELS
    public static String    LABEL_SLIDE_SHOW_TITLE = "slide_show_title";
    public static String    LABEL_LANGUAGE_SELECTION_PROMPT = "Select a Language:";
   
    public static String    CSS_CLASS_PAGEEDITOR_HORIZONTAL_TOOLBAR_PANE = "pageEditor_horizontal_toolbar_pane";
    public static String    CSS_CLASS_PAGEEDITOR_VERTICAL_TOOLBAR_PANE = "pageEditor_vertical_toolbar_pane";
}

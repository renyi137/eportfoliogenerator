/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.LanguagePropertyType.*;
import static epg.LanguagePropertyType.LABEL_HEADER_EDIT_PROMPT;
import static epg.LanguagePropertyType.OK_BUTTON_TEXT;

import static epg.StartupConstants.*;
import epg.model.HeaderComponent;
import epg.model.ImageComponent;
import epg.model.Page;
import java.io.File;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class ImageComponentEditorDialog extends Stage{
    GridPane gridPane;
    PageEditorView editorView;
    Button okButton;
    Button cancelButton;
    Button chooseImageButton;
    TextField imagePathTextField;
    Label imagePathLabel;
    TextField imageWidthTextField;
    Label imageWidthLabel;
    TextField imageHeightTextField;
    Label imageHeightLabel;
    Label captionLabel;
    TextField captionTextField;
    Label floatLabel;
    RadioButton leftButton;
    RadioButton rightButton;
    RadioButton neitherButton;
    ToggleGroup group;
    String path;
    String fileName;
    boolean isNewComponent;
    public ImageComponentEditorDialog(PageEditorView initEditorView, boolean initisNewComponent){
        isNewComponent=initisNewComponent;
        editorView=initEditorView;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        this.setTitle(props.getProperty(LABEL_IMAGE_EDIT_PROMPT));
	
	// SETUP THE PROMPT
	
	
	// INIT THE LANGUAGE CHOICES
	imagePathLabel=new Label("Path");
        imagePathTextField=new TextField();
        imageWidthLabel=new Label("Width");
        imageWidthTextField=new TextField();
        imageHeightLabel=new Label("Height");
        imageHeightTextField=new TextField();
        captionLabel=new Label("Caption");
        captionTextField=new TextField();
        chooseImageButton=new Button("choose");
        floatLabel=new Label("Float");
        leftButton=new RadioButton("Left");
        rightButton=new RadioButton("Right");
        neitherButton=new RadioButton("None");
	okButton = new Button(props.getProperty(OK_BUTTON_TEXT));
	cancelButton = new Button(props.getProperty(CANCEL_BUTTON_TEXT));
        group = new ToggleGroup();
        leftButton.setToggleGroup(group);
        rightButton.setToggleGroup(group);
        neitherButton.setToggleGroup(group);
        FlowPane floatPane=new FlowPane();
        floatPane.getChildren().add(leftButton);
        floatPane.getChildren().add(rightButton);
        floatPane.getChildren().add(neitherButton);
        
	gridPane = new GridPane();
	
	gridPane.add(imagePathLabel, 0, 0, 1, 1);
        gridPane.add(imagePathTextField, 1, 0, 1, 1);
        gridPane.add(chooseImageButton, 2, 0, 2, 1);
        gridPane.add(imageHeightLabel, 0, 1, 1, 1);
        gridPane.add(imageHeightTextField, 1, 1, 1, 1);
        gridPane.add(imageWidthLabel, 0, 2, 1, 1);
        gridPane.add(imageWidthTextField, 1, 2, 1, 1);
        gridPane.add(captionLabel, 0, 3, 1, 1);
        gridPane.add(captionTextField, 1, 3, 1, 1);
        gridPane.add(floatLabel, 0, 4, 1, 1);
        gridPane.add(floatPane, 1, 4, 1, 1);
        
	gridPane.add(okButton, 0, 5, 1, 1);
	gridPane.add(cancelButton, 2, 5, 1, 1);
        chooseImageButton.setOnAction(e -> {
        FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    fileName = file.getName();
        }
        imagePathTextField.setText(path+fileName);
	   
        });
	okButton.setOnAction(e -> {
	    Page editingPage=editorView.getPage();
            ImageComponent imageComponentToAdd;
            if(isNewComponent){
                imageComponentToAdd=new ImageComponent();
                imageComponentToAdd.setPath(path);
                imageComponentToAdd.setFileName(fileName);
                imageComponentToAdd.setWidth(imageWidthTextField.getText());
                imageComponentToAdd.setHeight(imageHeightTextField.getText());
                imageComponentToAdd.setWidth(imageWidthTextField.getText());
                imageComponentToAdd.setFloating(((RadioButton)group.getSelectedToggle()).getText());
                imageComponentToAdd.setCaption(captionTextField.getText());
                editingPage.addComponent(imageComponentToAdd);
                }
            else{
                imageComponentToAdd=(ImageComponent) editingPage.getSelectedComponent();
                imageComponentToAdd.setPath(path);
                imageComponentToAdd.setFileName(fileName);
                imageComponentToAdd.setWidth(imageWidthTextField.getText());
                imageComponentToAdd.setHeight(imageHeightTextField.getText());
                imageComponentToAdd.setWidth(imageWidthTextField.getText());
                imageComponentToAdd.setFloating(((RadioButton)group.getSelectedToggle()).getText());
                imageComponentToAdd.setCaption(captionTextField.getText());
            }
            editorView.reloadComponents();
	    this.close();
	});
	cancelButton.setOnAction(e -> {
	   
	    this.close();
	});
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
	imagePathLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        imageWidthLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        imageHeightLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        floatLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        leftButton.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        rightButton.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        neitherButton.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        captionLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
	okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        chooseImageButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        if(!this.isNewComponent){
            reloadImageData();
        }
        
        
    }
    public void reloadImageData(){
        ImageComponent imageComponentToLoad=(ImageComponent)editorView.getPage().getSelectedComponent();
        path=imageComponentToLoad.getPath();
        fileName=imageComponentToLoad.getFileName();
        imagePathTextField.setText(path+fileName);
        imageWidthTextField.setText(imageComponentToLoad.getWidth());
        imageHeightTextField.setText(imageComponentToLoad.getHeight());
        captionTextField.setText(imageComponentToLoad.getCaption());
        if(imageComponentToLoad.getFloating().equals("Left")){
        
        group.selectToggle(leftButton);
        }
        else if(imageComponentToLoad.getFloating().equals("Right")){
        group.selectToggle(rightButton);
        }
        else{
         group.selectToggle(neitherButton);
        }
    }
           
}



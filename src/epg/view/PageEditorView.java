/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.LanguagePropertyType;
import static epg.LanguagePropertyType.*;
import static epg.LanguagePropertyType.TOOLTIP_NEW;
import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.*;

import static epg.StartupConstants.PATH_ICONS;
import epg.controller.PageEditorController;
import epg.model.Component;
import epg.model.Page;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class PageEditorView extends BorderPane{
    VBox HEditorToolbarContainer;
    FlowPane HEditorToolbar1;
    FlowPane HEditorToolbar2;
    
    
    Label layoutLabel;
    ComboBox<String> layoutComboBox;
    Label colorTemplateLabel;
    ComboBox<String> colorTemplateComboBox;
    Button bannerImageButton;
    Label fontsFamilyLabel;
    Label fontsSizeLabel;
    ComboBox<String> fontsFamilyComboBox;
    
    ComboBox<String> fontsSizeComboBox;
    Button boldButton;
    Button italicButton;
    Button underlinedButton;
    FlowPane VEditorToolbar;
    Label title;
    TextField titleText;
    Label studentName;
    TextField nameText;
    Label footer;
    TextField footerText;
    Button addHeaderButton;
    Button addListButton;
    Button addParagraphButton;
    Button addImageButton;
    Button addSlideshowButton;
    Button addVideoButton;
    Button removeButton;
    Button editButton;
    Button addHyperlinkButton;
    
    ScrollPane pageEditorScrollPane;
    VBox pageEditorVBox;
    BorderPane pagePlace;
    ImageView bannerImage;
    VBox banner;
    FlowPane navbar;
    private Page page;
    EPortfolioGeneratorView ui;
    private PageEditorController pageEditorController;
    private boolean isBold;
    private boolean isItalic;
    private boolean isUnderlined;
    public PageEditorView(EPortfolioGeneratorView initUI){
        ui=initUI;
        isBold=false;
        isItalic=false;
        isUnderlined=false;
        initToolbar();
        initPageEditorSpace();
        initEventHandler();
    }
    public Page getPage(){
        return page;
    }
    
    public void initEventHandler(){
        pageEditorController=new PageEditorController(this);
        addHeaderButton.setOnAction(e ->{
            pageEditorController.handleAddHeaderRequest(true);
            updateToolbar();
            ui.markFileAsNotSaved();
        });
        addListButton.setOnAction(e ->{
            pageEditorController.handleAddListRequest(true);
            updateToolbar();
            ui.markFileAsNotSaved();
        });
        addParagraphButton.setOnAction(e ->{
            pageEditorController.handleAddParagraphRequest(true);
            updateToolbar();
            ui.markFileAsNotSaved();
        });
        addImageButton.setOnAction(e ->{
            pageEditorController.handleAddImageRequest(true);
            updateToolbar();
            ui.markFileAsNotSaved();
        });
        addVideoButton.setOnAction(e ->{
            pageEditorController.handleAddVideoRequest(true);
            updateToolbar();
            ui.markFileAsNotSaved();
        });
        addSlideshowButton.setOnAction(e ->{
            pageEditorController.handleAddSlideshowRequest(true);
            updateToolbar();
            ui.markFileAsNotSaved();
        });
        addHyperlinkButton.setOnAction(e ->{
            pageEditorController.handleAddHyperlinkRequest();
            ui.markFileAsNotSaved();
            
        });
        editButton.setOnAction(e ->{
            pageEditorController.handleEditRequest();
            ui.markFileAsNotSaved();
        });
        removeButton.setOnAction(e ->{
            pageEditorController.handleRemoveRequest();
            updateToolbar();
            ui.markFileAsNotSaved();
        });
        titleText.textProperty().addListener(e -> {
	    page.setTitle(titleText.getText());
	    ui.reloadPageSelectPane();
            ui.markFileAsNotSaved();
	});
        nameText.textProperty().addListener(e -> {
	    page.setStudentName(nameText.getText());
            ui.markFileAsNotSaved();
	    
	});
        footerText.textProperty().addListener(e -> {
	    page.setFooter(footerText.getText());
            ui.markFileAsNotSaved();
	});
        boldButton.setOnAction(e -> {
            if(isBold){
                isBold=false;
                boldButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED);
                page.setIsBold(isBold);
            }
            else{
                isBold=true;
                boldButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED);
                page.setIsBold(isBold);
            }
            ui.markFileAsNotSaved();
        });
        italicButton.setOnAction(e -> {
            if(isItalic){
                isItalic=false;
                italicButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED);
                page.setIsItalic(isItalic);
            }
            else{
                isItalic=true;
                italicButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
                page.setIsItalic(isItalic);
            }
            ui.markFileAsNotSaved();
        });
        underlinedButton.setOnAction(e -> {
            if(isUnderlined){
                isUnderlined=false;
                underlinedButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED);
                page.setIsUnderlined(isUnderlined);
            }
            else{
                isUnderlined=true;
                underlinedButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED);
                page.setIsUnderlined(isUnderlined);
            }
            ui.markFileAsNotSaved();
        });
        fontsFamilyComboBox.setOnAction(e ->{
            page.setFontFamily(fontsFamilyComboBox.getSelectionModel().getSelectedItem().toString());
            ui.markFileAsNotSaved();
        });
        fontsSizeComboBox.setOnAction(e ->{
            page.setFontSize(fontsSizeComboBox.getSelectionModel().getSelectedItem().toString());
            ui.markFileAsNotSaved();
        });
        bannerImageButton.setOnAction(e ->{
            pageEditorController.handleChangeBannerImageRequest();
            ui.markFileAsNotSaved();
        });
        layoutComboBox.setOnAction(e ->{
            page.setLayout(layoutComboBox.getSelectionModel().getSelectedItem().toString());
            reloadPage();
            ui.markFileAsNotSaved();
        });
        colorTemplateComboBox.setOnAction(e ->{
            page.setColorTemplate(colorTemplateComboBox.getSelectionModel().getSelectedItem().toString());
            reloadPage();
            ui.markFileAsNotSaved();
        });
    }
    public void initToolbar(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        HEditorToolbarContainer=new VBox();
        VEditorToolbar=new FlowPane();
        HEditorToolbar1=new FlowPane();
        HEditorToolbar2=new FlowPane();
        title=new Label(props.getProperty(LABEL_PAGE_TITLE));
        studentName=new Label(props.getProperty(LABEL_STUDENT_NAME));
        footer=new Label(props.getProperty(LABEL_PAGE_FOOTER));
        titleText=new TextField();
        nameText=new TextField();
        footerText=new TextField();
        HEditorToolbar1.getChildren().add(title);
        HEditorToolbar1.getChildren().add(titleText);
        HEditorToolbar1.getChildren().add(studentName);
        HEditorToolbar1.getChildren().add(nameText);
        HEditorToolbar1.getChildren().add(footer);
        HEditorToolbar1.getChildren().add(footerText);
        
        
        fontsFamilyLabel = new Label(props.getProperty(LABEL_FONTS_FAMILY.toString()));
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> fontsFamilyChoices = FXCollections.observableArrayList();
	fontsFamilyChoices.add(FONTS_FAMILY_1);
	fontsFamilyChoices.add(FONTS_FAMILY_2);
        fontsFamilyChoices.add(FONTS_FAMILY_3);
        fontsFamilyChoices.add(FONTS_FAMILY_4);
        fontsFamilyChoices.add(FONTS_FAMILY_5);
	fontsFamilyComboBox = new ComboBox(fontsFamilyChoices);
	fontsFamilyComboBox.getSelectionModel().select(FONTS_FAMILY_1);
	HEditorToolbar2.getChildren().add(fontsFamilyLabel);
        HEditorToolbar2.getChildren().add(fontsFamilyComboBox);
        
        fontsSizeLabel = new Label(props.getProperty(LABEL_FONTS_SIZE.toString()));
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> fontsSizeChoices = FXCollections.observableArrayList();
	fontsSizeChoices.add(FONTS_SIZE_1);
	fontsSizeChoices.add(FONTS_SIZE_2);
        fontsSizeChoices.add(FONTS_SIZE_3);
        fontsSizeChoices.add(FONTS_SIZE_4);
        fontsSizeChoices.add(FONTS_SIZE_5);
	fontsSizeComboBox = new ComboBox(fontsSizeChoices);
	fontsSizeComboBox.getSelectionModel().select(FONTS_SIZE_1);
	HEditorToolbar2.getChildren().add(fontsSizeLabel);
        HEditorToolbar2.getChildren().add(fontsSizeComboBox);
        boldButton=initChildButton(HEditorToolbar2, ICON_BOLD, TOOLTIP_BOLD,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        italicButton=initChildButton(HEditorToolbar2, ICON_ITALIC, TOOLTIP_ITALIC,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        underlinedButton=initChildButton(HEditorToolbar2, ICON_UNDERLINED, TOOLTIP_UNDERLINED,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        
        bannerImageButton=initChildButton(HEditorToolbar1, ICON_BANNER_IMAGE, TOOLTIP_BANNER_IMAGE,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        layoutLabel=new Label("Layout");
        colorTemplateLabel=new Label("Color Template");
        ObservableList<String> layoutChoices = FXCollections.observableArrayList();
	layoutChoices.add(LAYOUT_1);
	layoutChoices.add(LAYOUT_2);
        layoutChoices.add(LAYOUT_3);
        layoutChoices.add(LAYOUT_4);
        layoutChoices.add(LAYOUT_5);
	layoutComboBox = new ComboBox(layoutChoices);
	layoutComboBox.getSelectionModel().select(LAYOUT_1);
	HEditorToolbar2.getChildren().add(layoutLabel);
        HEditorToolbar2.getChildren().add(layoutComboBox);
        
        ObservableList<String> colorTemplateChoices = FXCollections.observableArrayList();
	colorTemplateChoices.add(COLOR_TEMPLATE_1);
	colorTemplateChoices.add(COLOR_TEMPLATE_2);
        colorTemplateChoices.add(COLOR_TEMPLATE_3);
        colorTemplateChoices.add(COLOR_TEMPLATE_4);
        colorTemplateChoices.add(COLOR_TEMPLATE_5);
	colorTemplateComboBox = new ComboBox(colorTemplateChoices);
	colorTemplateComboBox.getSelectionModel().select(COLOR_TEMPLATE_1);
	HEditorToolbar2.getChildren().add(colorTemplateLabel);
        HEditorToolbar2.getChildren().add(colorTemplateComboBox);
       
        addHeaderButton=initChildButton(VEditorToolbar, ICON_ADD_HEADER, TOOLTIP_ADD_HEADER,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        addListButton=initChildButton(VEditorToolbar, ICON_ADD_LIST, TOOLTIP_ADD_LIST,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        addParagraphButton=initChildButton(VEditorToolbar, ICON_ADD_PARAGRAPH, TOOLTIP_ADD_PARAGRAPH,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        addImageButton=initChildButton(VEditorToolbar, ICON_ADD_IMAGE, TOOLTIP_ADD_IMAGE,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        addSlideshowButton=initChildButton(VEditorToolbar, ICON_ADD_SLIDESHOW, TOOLTIP_ADD_SLIDESHOW,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        addVideoButton=initChildButton(VEditorToolbar, ICON_ADD_VIDEO, TOOLTIP_ADD_VIDEO,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        addHyperlinkButton=initChildButton(VEditorToolbar, ICON_ADD_HYPERLINK, TOOLTIP_ADD_HYPERLINK,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        removeButton=initChildButton(VEditorToolbar, ICON_REMOVE_COMPONENT, TOOLTIP_REMOVE_COMPONENT,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        editButton=initChildButton(VEditorToolbar, ICON_EDIT_COMPONENT, TOOLTIP_EDIT_COMPONENT,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        
        HEditorToolbarContainer.getChildren().add(HEditorToolbar1);
        HEditorToolbarContainer.getChildren().add(HEditorToolbar2);
        this.setTop(HEditorToolbarContainer);
        this.setLeft(VEditorToolbar);
        
        title.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        footer.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        studentName.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        fontsSizeLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        fontsFamilyLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        layoutLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        colorTemplateLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        fontsSizeLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        HEditorToolbar1.getStyleClass().add(CSS_CLASS_PAGEEDITOR_HORIZONTAL_TOOLBAR_PANE);
        HEditorToolbar2.getStyleClass().add(CSS_CLASS_PAGEEDITOR_HORIZONTAL_TOOLBAR_PANE);
        VEditorToolbar.getStyleClass().add(CSS_CLASS_PAGEEDITOR_VERTICAL_TOOLBAR_PANE);
        
       
    }
    public void initPageEditorSpace(){
        pagePlace=new BorderPane();
        bannerImage=new ImageView();
        pageEditorVBox=new VBox();
        navbar=new FlowPane();
        banner=new VBox();
        pageEditorScrollPane=new ScrollPane(pageEditorVBox);
        
        
        this.setCenter(pagePlace);
  
    }
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    public void reloadComponents(){
        pageEditorVBox.getChildren().clear();
        
        //page=pageToLoad;
        for (Component component : page.getComponents()) {
	    ComponentView componentView = new ComponentView(this, component);
	    if (page.isSelectedComponent(component)){
		componentView.getStyleClass().add(CSS_CLASS_SELECTED_COMPONENT_EDIT_VIEW);
            }
            else{
		componentView.getStyleClass().add(CSS_CLASS_COMPONENT_EDIT_VIEW);
            }
	    pageEditorVBox.getChildren().add(componentView);;
	    componentView.setOnMousePressed(e -> {
		page.setSelectedComponent(component);
		this.reloadComponents();
                updateToolbar();
	    });
	}
    }
    public void reloadPageEditorView(){
        page=ui.getSite().getSelectedPage();
        titleText.setText(page.getTitle());
        nameText.setText(page.getStudentName());
        footerText.setText(page.getFooter());
        fontsFamilyComboBox.getSelectionModel().select(page.getFontFamily());
        fontsSizeComboBox.getSelectionModel().select(page.getFontSize());
       
        layoutComboBox.getSelectionModel().select(page.getLayout());
        colorTemplateComboBox.getSelectionModel().select(page.getColorTemplate());
        reloadFontStyle(page);
        reloadPage();
    }
    public void  reloadFontStyle(Page page){
        if(!isBold==page.isIsBold()){
            if(isBold){
               isBold=false;
                boldButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED); 
            }
            else{
               isBold=true;
                boldButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
            }
        }
        if(!isItalic==page.isIsItalic()){
            if(isItalic){
               isItalic=false;
                italicButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED); 
            }
            else{
               isItalic=true;
                italicButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
            }
        }
        if(!isUnderlined==page.isIsUnderlined()){
            if(isUnderlined){
               isUnderlined=false;
                underlinedButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED); 
            }
            else{
               isUnderlined=true;
                underlinedButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
            }
        }
    }
    public void reloadPage(){
        
        
        pagePlace.getStyleClass().clear();
        pagePlace.getChildren().clear();
        banner.getChildren().clear();
        reloadBannerImage();
        reloadNavbar();
        switch(page.getLayout()){
            case "layout1": banner.getChildren().add(navbar);
                            banner.getChildren().add(bannerImage);
                            break;
            case "layout2": 
                            banner.getChildren().add(bannerImage);
                            banner.getChildren().add(navbar);
                            break;
            case "layout3": banner.getChildren().add(bannerImage);
                            pagePlace.setBottom(navbar);
                            break;
            case "layout4": banner.getChildren().add(bannerImage);
                            pagePlace.setLeft(navbar);
                            break;
            case "layout5": banner.getChildren().add(bannerImage);
                            pagePlace.setRight(navbar);
                            break;
        }
        switch(page.getColorTemplate()){
            case "color1": pagePlace.getStyleClass().add(CSS_CLASS_PAGE_COLOR_1);break;
            case "color2": pagePlace.getStyleClass().add(CSS_CLASS_PAGE_COLOR_2);break;
            case "color3": pagePlace.getStyleClass().add(CSS_CLASS_PAGE_COLOR_3);break;
            case "color4": pagePlace.getStyleClass().add(CSS_CLASS_PAGE_COLOR_4);break;
            case "color5": pagePlace.getStyleClass().add(CSS_CLASS_PAGE_COLOR_5);break;
            
        }
        reloadComponents();
        pagePlace.setTop(banner);
        pagePlace.setCenter(pageEditorScrollPane);
    }
    public void reloadNavbar(){
        navbar.getChildren().clear();
        for (Page tempPage : ui.getSite().getPages()) {
            navbar.getChildren().add(new Label(tempPage.getTitle()));
            
        }
  
    }
    public void reloadBannerImage(){
        String imagePath = page.getBannerImagePath() + page.getBannerImageFileName();
	File file = new File(imagePath);
	    // GET AND SET THE IMAGE
	    URL fileURL;
        try {
            fileURL = file.toURI().toURL();
       
	    Image image = new Image(fileURL.toExternalForm());
	    bannerImage.setImage(image);
            } catch (MalformedURLException ex) {
            Logger.getLogger(PageEditorView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void updateToolbar(){
        if(page.getComponents().isEmpty()||page.getSelectedComponent()==null){
            removeButton.setDisable(true);
        }
        else{
            removeButton.setDisable(false);
        }
        if(page.getSelectedComponent()==null){
            editButton.setDisable(true);
        }
        else{
            editButton.setDisable(false);
        }
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.LanguagePropertyType.*;
import static epg.LanguagePropertyType.LABEL_IMAGE_EDIT_PROMPT;
import static epg.LanguagePropertyType.OK_BUTTON_TEXT;
import static epg.StartupConstants.*;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.ImageComponent;
import epg.model.Page;
import epg.model.VideoComponent;
import java.io.File;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class VideoComponentEditorDialog extends Stage{
    
    GridPane gridPane;
    PageEditorView editorView;
    Button okButton;
    Button cancelButton;
    Button chooseVideoButton;
    TextField videoPathTextField;
    Label videoPathLabel;
     TextField videoWidthTextField;
    Label videoWidthLabel;
     TextField videoHeightTextField;
    Label videoHeightLabel;
    Label captionLabel;
    TextField captionTextField;
    String path;
    String fileName;
    boolean isNewComponent;
    public VideoComponentEditorDialog(PageEditorView initEditorView, boolean initisNewComponent){
        isNewComponent=initisNewComponent;
        editorView=initEditorView;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        this.setTitle(props.getProperty(LABEL_VIDEO_EDIT_PROMPT));
	
	// SETUP THE PROMPT
	
	
	// INIT THE LANGUAGE CHOICES
	videoPathLabel=new Label("Path");
        videoPathTextField=new TextField();
        videoWidthLabel=new Label("Width");
        videoWidthTextField=new TextField();
        videoHeightLabel=new Label("Hight");
        videoHeightTextField=new TextField();
        captionLabel=new Label("Caption");
        captionTextField=new TextField();
        chooseVideoButton=new Button("choose");
	okButton = new Button(props.getProperty(OK_BUTTON_TEXT));
	cancelButton = new Button(props.getProperty(CANCEL_BUTTON_TEXT));
	gridPane = new GridPane();
	
	gridPane.add(videoPathLabel, 0, 0, 1, 1);
        gridPane.add(videoPathTextField, 1, 0, 1, 1);
        gridPane.add(chooseVideoButton, 2, 0, 2, 1);
        gridPane.add(videoHeightLabel, 0, 1, 1, 1);
        gridPane.add(videoHeightTextField, 1, 1, 1, 1);
        gridPane.add(videoWidthLabel, 0, 2, 1, 1);
        gridPane.add(videoWidthTextField, 1, 2, 1, 1);
        gridPane.add(captionLabel, 0, 3, 1, 1);
        gridPane.add(captionTextField, 1, 3, 1, 1);
	
	gridPane.add(okButton, 0, 4, 1, 1);
	gridPane.add(cancelButton, 2, 4, 1, 1);
        chooseVideoButton.setOnAction(e -> {
        FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("AVI files (*.avi)", "*.AVI");
        FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("RMVB files (*.rmvb)", "*.RMVB");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    fileName = file.getName();
        }
        videoPathTextField.setText(path+fileName);
	   
        });
	okButton.setOnAction(e -> {
	   Page editingPage=editorView.getPage();
            VideoComponent videoComponentToAdd;
            if(isNewComponent){
                videoComponentToAdd=new VideoComponent();
                videoComponentToAdd.setPath(path);
                videoComponentToAdd.setFileName(fileName);
                videoComponentToAdd.setWidth(videoWidthTextField.getText());
                videoComponentToAdd.setHeight(videoHeightTextField.getText());
                videoComponentToAdd.setWidth(videoWidthTextField.getText());
                
                videoComponentToAdd.setCaption(captionTextField.getText());
                editingPage.addComponent(videoComponentToAdd);
                }
            else{
                videoComponentToAdd=(VideoComponent) editingPage.getSelectedComponent();
                videoComponentToAdd.setPath(path);
                videoComponentToAdd.setFileName(fileName);
                videoComponentToAdd.setWidth(videoWidthTextField.getText());
                videoComponentToAdd.setHeight(videoHeightTextField.getText());
                videoComponentToAdd.setWidth(videoWidthTextField.getText());
                
                videoComponentToAdd.setCaption(captionTextField.getText());
            }
            editorView.reloadComponents();
	    this.close();
	    
	});
	cancelButton.setOnAction(e -> {
	   
	    this.close();
	});
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        
	videoPathLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        videoWidthLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        videoHeightLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        captionLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
	okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        chooseVideoButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        if(!isNewComponent){
            reloadVideoData();
        }
    }
    public void reloadVideoData(){
        VideoComponent videoComponentToLoad=(VideoComponent)editorView.getPage().getSelectedComponent();
        path=videoComponentToLoad.getPath();
        fileName=videoComponentToLoad.getFileName();
        videoPathTextField.setText(path+fileName);
        videoWidthTextField.setText(videoComponentToLoad.getWidth());
        videoHeightTextField.setText(videoComponentToLoad.getHeight());
        captionTextField.setText(videoComponentToLoad.getCaption());
       
    }   
}





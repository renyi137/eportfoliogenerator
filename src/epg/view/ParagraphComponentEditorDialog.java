/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.LanguagePropertyType;
import static epg.LanguagePropertyType.*;
import static epg.LanguagePropertyType.LABEL_HEADER_EDIT_PROMPT;
import static epg.LanguagePropertyType.OK_BUTTON_TEXT;
import static epg.StartupConstants.*;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.ListComponent;
import epg.model.Page;
import epg.model.ParagraphComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class ParagraphComponentEditorDialog extends Stage{
    GridPane gridPane;
    PageEditorView editorView;
    Button okButton;
    Button cancelButton;
    TextArea paragraphTextField;
    ScrollPane paragraphScrollPane;
    FlowPane toolbar;
    private Label fontsFamilyLabel;
    private  ComboBox fontsFamilyComboBox;
    private Label fontsSizeLabel;
    private ComboBox fontsSizeComboBox;
    private Button boldButton;
    private Button italicButton;
    private Button underlinedButton;
    private Button addHyperlinkButton;
     private boolean isNewComponent;
    private boolean isBold;
    private boolean isItalic;
    private boolean isUnderlined;
    public ParagraphComponentEditorDialog(PageEditorView initEditorView, boolean initisNewComponent){
        isNewComponent=initisNewComponent;
        editorView=initEditorView;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        this.setTitle(props.getProperty(LABEL_PARAGRAPH_EDIT_PROMPT));
	toolbar=new FlowPane();
	// SETUP THE PROMPT
	fontsFamilyLabel = new Label(props.getProperty(LABEL_FONTS_FAMILY.toString()));
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> fontsFamilyChoices = FXCollections.observableArrayList();
	fontsFamilyChoices.add(FONTS_FAMILY_1);
	fontsFamilyChoices.add(FONTS_FAMILY_2);
        fontsFamilyChoices.add(FONTS_FAMILY_3);
        fontsFamilyChoices.add(FONTS_FAMILY_4);
        fontsFamilyChoices.add(FONTS_FAMILY_5);
	fontsFamilyComboBox = new ComboBox(fontsFamilyChoices);
	fontsFamilyComboBox.getSelectionModel().select(FONTS_FAMILY_1);
	toolbar.getChildren().add(fontsFamilyLabel);
        toolbar.getChildren().add(fontsFamilyComboBox);
        
        fontsSizeLabel = new Label(props.getProperty(LABEL_FONTS_SIZE.toString()));
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> fontsSizeChoices = FXCollections.observableArrayList();
	fontsSizeChoices.add(FONTS_SIZE_1);
	fontsSizeChoices.add(FONTS_SIZE_2);
        fontsSizeChoices.add(FONTS_SIZE_3);
        fontsSizeChoices.add(FONTS_SIZE_4);
        fontsSizeChoices.add(FONTS_SIZE_5);
	fontsSizeComboBox = new ComboBox(fontsSizeChoices);
	fontsSizeComboBox.getSelectionModel().select(FONTS_SIZE_1);
	toolbar.getChildren().add(fontsSizeLabel);
        toolbar.getChildren().add(fontsSizeComboBox);
        boldButton=initChildButton(toolbar, ICON_BOLD, TOOLTIP_BOLD,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        italicButton=initChildButton(toolbar, ICON_ITALIC, TOOLTIP_ITALIC,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        underlinedButton=initChildButton(toolbar, ICON_UNDERLINED, TOOLTIP_UNDERLINED,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	
	// INIT THE LANGUAGE CHOICES
	paragraphTextField=new TextArea();
        paragraphTextField.autosize();
        paragraphTextField.setMinWidth(570);
        paragraphTextField.setMaxWidth(580);
        paragraphTextField.setWrapText(true);
        paragraphScrollPane=new ScrollPane(paragraphTextField);
        paragraphScrollPane.setMinWidth(570);
	okButton = new Button(props.getProperty(OK_BUTTON_TEXT));
	cancelButton = new Button(props.getProperty(CANCEL_BUTTON_TEXT));
	gridPane = new GridPane();
	gridPane.add(toolbar,0,0,1,1);
	gridPane.add(paragraphScrollPane, 0, 1, 1, 1);
	gridPane.add(okButton, 0, 2, 1, 1);
	gridPane.add(cancelButton, 2, 2, 1, 1);
        if(!this.isNewComponent){
            reloadParagraphPane();
        }
        boldButton.setOnAction(e -> {
            if(isBold){
                isBold=false;
                boldButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED);
            }
            else{
                isBold=true;
                boldButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED);  
            }
        });
        italicButton.setOnAction(e -> {
            if(isItalic){
                isItalic=false;
                italicButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED);
            }
            else{
                isItalic=true;
                italicButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED);  
            }
        });
        underlinedButton.setOnAction(e -> {
            if(isUnderlined){
                isUnderlined=false;
                underlinedButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED);
            }
            else{
                isUnderlined=true;
                underlinedButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED);  
            }
        });
	okButton.setOnAction(e -> {
	    Page editingPage=editorView.getPage();
            ParagraphComponent paragraphToAdd;
            if(isNewComponent){
                paragraphToAdd=new ParagraphComponent();
                paragraphToAdd.setContent(paragraphTextField.getText());
                paragraphToAdd.setFontFamily(fontsFamilyComboBox.getSelectionModel().getSelectedItem().toString());
                paragraphToAdd.setFontSize(fontsSizeComboBox.getSelectionModel().getSelectedItem().toString());
                paragraphToAdd.setIsBold(isBold);
                paragraphToAdd.setIsItalic(isItalic);
                paragraphToAdd.setIsUnderlined(isUnderlined);
                editingPage.addComponent(paragraphToAdd);
                }
            else{
                paragraphToAdd=(ParagraphComponent)editingPage.getSelectedComponent();
                paragraphToAdd.setContent(paragraphTextField.getText());
                 paragraphToAdd.setFontFamily(fontsFamilyComboBox.getSelectionModel().getSelectedItem().toString());
                paragraphToAdd.setFontSize(fontsSizeComboBox.getSelectionModel().getSelectedItem().toString());
                paragraphToAdd.setIsBold(isBold);
                paragraphToAdd.setIsItalic(isItalic);
                paragraphToAdd.setIsUnderlined(isUnderlined);
                editingPage.addComponent(paragraphToAdd);
            }
            editorView.reloadComponents();
	    this.close();
	});
	cancelButton.setOnAction(e -> {
	   
	    this.close();
	});
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
	fontsFamilyLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        fontsSizeLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        toolbar.getStyleClass().add(CSS_CLASS_PAGEEDITOR_HORIZONTAL_TOOLBAR_PANE);
	okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
    }
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    public void reloadParagraphPane(){
        ParagraphComponent paragraph=(ParagraphComponent)(editorView.getPage().getSelectedComponent());
        if(paragraph.isIsBold()){
            isBold=true;
            boldButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
        }
        if(paragraph.isIsItalic()){
            isItalic=true;
            italicButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
        }
        if(paragraph.isIsUnderlined()){
            isUnderlined=true;
            underlinedButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
        }
        fontsSizeComboBox.getSelectionModel().select(paragraph.getFontSize());
        fontsFamilyComboBox.getSelectionModel().select(paragraph.getFontFamily());
    }
    
           
}


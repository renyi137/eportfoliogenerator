/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.LanguagePropertyType;
import static epg.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static epg.StartupConstants.*;
import epg.controller.FileController;

import epg.error.ErrorHandler;
import epg.model.Component;
import epg.model.HeaderComponent;
import epg.model.ImageComponent;
import epg.model.Item;
import epg.model.ListComponent;
import epg.model.ParagraphComponent;
import epg.model.SlideShowModel;
import epg.model.VideoComponent;
import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *
 * @author yiren
 */
public class ComponentView extends VBox{
    Component component;
    PageEditorView pageEditor;
    public ComponentView(PageEditorView initPageEditor,Component initComponent){
        pageEditor=initPageEditor;
        component=initComponent;
        if(component instanceof HeaderComponent){
            Text header=new Text(((HeaderComponent)component).getContent());
            
            getChildren().add(header);
            
            
        }
        if(component instanceof ListComponent){
            for(Item listItem: ((ListComponent)component).getList()){
                getChildren().add(new Label(listItem.getContent()));
            }
        }
        if(component instanceof ParagraphComponent){
            Text paragraph=new Text(((ParagraphComponent)component).getContent());
            getChildren().add(paragraph);
        }
        if(component instanceof ImageComponent){
            ImageComponent imageComponent=(ImageComponent)component;
            String imagePath = imageComponent.getPath() + imageComponent.getFileName();
            VBox imageContainer=new VBox();
            Label caption=new Label(imageComponent.getCaption());
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image componentImage = new Image(fileURL.toExternalForm());
	    ImageView imageView=new ImageView(componentImage);
	    
	    // AND RESIZE IT
	    System.out.println(imageComponent.getWidth());
	    imageView.setFitWidth(Integer.parseInt(imageComponent.getWidth()));
	    imageView.setFitHeight(Integer.parseInt(imageComponent.getHeight()));
            imageContainer.getChildren().add(imageView);
            imageContainer.getChildren().add(caption);
            getChildren().add(imageContainer);
	} catch (Exception e) {
             Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, e);
	    //ErrorHandler eH = new ErrorHandler(null);
            //eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
	}
        }
        if(component instanceof VideoComponent){
            VideoComponent videoComponent=(VideoComponent)component;
            String imagePath =PATH_IMAGES+"DefaultVideoImage.png";
            VBox imageContainer=new VBox();
            Label caption=new Label(videoComponent.getCaption());
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image componentImage = new Image(fileURL.toExternalForm());
	    ImageView imageView=new ImageView(componentImage);
	    
	    // AND RESIZE IT
	   
	    imageView.setFitWidth(Integer.parseInt(videoComponent.getWidth()));
	    imageView.setFitHeight(Integer.parseInt(videoComponent.getHeight()));
            imageContainer.getChildren().add(imageView);
            imageContainer.getChildren().add(caption);
            getChildren().add(imageContainer);
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
	}
        }
        if(component instanceof SlideShowModel){
            SlideShowModel videoComponent=(SlideShowModel)component;
            String imagePath =PATH_IMAGES+"DefaultSlideShowImage.png";
            VBox imageContainer=new VBox();
            Label title=new Label(videoComponent.getTitle());
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image componentImage = new Image(fileURL.toExternalForm());
	    ImageView imageView=new ImageView(componentImage);
	    
	    // AND RESIZE IT
	   
	    imageContainer.getChildren().add(title);
            imageContainer.getChildren().add(imageView);
            
            getChildren().add(imageContainer);
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
	}
        }
    }
    
    
}

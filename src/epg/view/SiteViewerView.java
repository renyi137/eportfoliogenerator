/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.model.Page;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author yiren
 */
public class SiteViewerView extends BorderPane{
    EPortfolioGeneratorView ui;
    WebView webView;
    WebEngine webEngine;
    public SiteViewerView(EPortfolioGeneratorView initUI){
        ui=initUI;
        
    }
    public void reloadPage(){
        initSiteViewer();
    }
    public void initSiteViewer(){
        webView = new WebView();
	this.setCenter(webView);
        File indexFile = new File("./sites/test/index.html");
	URL indexURL;
        try {
            indexURL = indexFile.toURI().toURL();
            webEngine = webView.getEngine();
	webEngine.load(indexURL.toExternalForm());
	webEngine.setJavaScriptEnabled(true);
        } catch (MalformedURLException ex) {
            Logger.getLogger(SiteViewerView.class.getName()).log(Level.SEVERE, null, ex);
        }
	
	// SETUP THE WEB ENGINE AND LOAD THE URL
	
    }
}

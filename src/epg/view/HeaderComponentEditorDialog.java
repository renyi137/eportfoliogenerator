/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.LanguagePropertyType.*;
import static epg.StartupConstants.CHINESE_LANG;
import static epg.StartupConstants.CSS_CLASS_LANG_COMBO_BOX;
import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.ENGLISH_LANG;
import static epg.StartupConstants.LABEL_LANGUAGE_SELECTION_PROMPT;

import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.HeaderComponent;
import epg.model.Page;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class HeaderComponentEditorDialog extends Stage{
    GridPane gridPane;
    PageEditorView editorView;
    Button okButton;
    Button cancelButton;
    TextField HeaderTextField;
    Label HeaderEditPromptLabel;
    private boolean isNewCoponent;
    public HeaderComponentEditorDialog(PageEditorView initEditorView, boolean isNewComponent){
        this.isNewCoponent=isNewComponent;
        editorView=initEditorView;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        this.setTitle(props.getProperty(LABEL_HEADER_EDIT_PROMPT));
	
	// SETUP THE PROMPT
	HeaderEditPromptLabel = new Label(props.getProperty(LABEL_HEADER_EDIT_PROMPT));
	
	// INIT THE LANGUAGE CHOICES
	HeaderTextField=new TextField();
        if(!isNewComponent){
            reloadHeaderData();
        }
	okButton = new Button(props.getProperty(OK_BUTTON_TEXT));
	cancelButton = new Button(props.getProperty(CANCEL_BUTTON_TEXT));
	gridPane = new GridPane();
	
	gridPane.add(HeaderTextField, 0, 0, 2, 1);
	gridPane.add(okButton, 0, 1, 1, 1);
	gridPane.add(cancelButton, 1, 1, 1, 1);
	okButton.setOnAction(e -> {
	    Page editingPage=editorView.getPage();
            if(isNewComponent){
                editingPage.addHeaderComponent(HeaderTextField.getText());
                }
            else{
                HeaderComponent editingHeaderComponent=(HeaderComponent) editingPage.getSelectedComponent();
                editingHeaderComponent.setContent(HeaderTextField.getText());
            }
            editorView.reloadComponents();
	    this.close();
	});
	cancelButton.setOnAction(e -> {
	   
	    this.close();
	});
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
	
	okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
    }
    public void reloadHeaderData(){
        HeaderComponent selectedComponent=(HeaderComponent)editorView.getPage().getSelectedComponent();
        HeaderTextField.setText(selectedComponent.getContent());
    }
           
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

/**
 *
 * @author yiren
 */


import static epg.LanguagePropertyType.*;
import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import epg.model.Item;
import epg.model.ListComponent;
import epg.model.ParagraphComponent;



import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class HyperlinkAddDialog extends Stage{
     GridPane gridPane;
    PageEditorView editorView;
    Button okButton;
    Button cancelButton;
    TextField HyperlinkTextField;
    TextArea editText;
    Label HyperlinkEditPromptLabel;
    ScrollPane editTextScrollPane;
    public HyperlinkAddDialog(PageEditorView initEditorView){
        editorView=initEditorView;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        this.setTitle(props.getProperty(LABEL_HYPERLINK_EDIT_PROMPT));
	
	// SETUP THE PROMPT
	HyperlinkEditPromptLabel = new Label("URL");
	
	// INIT THE LANGUAGE CHOICES
	HyperlinkTextField=new TextField();
        editText=new TextArea();
        editText.setEditable(false);
        editTextScrollPane=new ScrollPane(editText);
        reloadContent();
	okButton = new Button(props.getProperty(OK_BUTTON_TEXT));
	cancelButton = new Button(props.getProperty(CANCEL_BUTTON_TEXT));
	gridPane = new GridPane();
        gridPane.add(editTextScrollPane, 0, 0, 2, 1);
	gridPane.add(HyperlinkEditPromptLabel, 0, 1, 1, 1);
	gridPane.add(HyperlinkTextField, 1, 1, 2, 1);
	gridPane.add(okButton, 0, 2, 1, 1);
	gridPane.add(cancelButton, 1, 2, 1, 1);
	okButton.setOnAction(e -> {
            
	    String source=editText.getSelectedText();
            
            
	    this.close();
	});
	cancelButton.setOnAction(e -> {
	   
	    this.close();
	});
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
	HyperlinkEditPromptLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
	okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
    }
    public void reloadContent(){
        Component componentToLoad=editorView.getPage().getSelectedComponent();
        if(componentToLoad instanceof ListComponent){
            ListComponent listComponentToLoad=(ListComponent)componentToLoad;
            String text="";
            for(Item item: listComponentToLoad.getList()  ){
                text+=item.getContent()+"\n";
                
            }
            editText.setText(text);
                
        }
        if(componentToLoad instanceof ParagraphComponent){
            ParagraphComponent paragraphComponentToLoad=(ParagraphComponent)componentToLoad;
            
            editText.setText(paragraphComponentToLoad.getContent());
                
        }
        
    }
           
}


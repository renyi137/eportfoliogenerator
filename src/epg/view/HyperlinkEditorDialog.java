/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.LanguagePropertyType.*;
import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.STYLE_SHEET_UI;



import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class HyperlinkEditorDialog extends Stage{
     GridPane gridPane;
    PageEditorView editorView;
    Button okButton;
    Button cancelButton;
    TextField HyperlinkTextField;
    Label HyperlinkEditPromptLabel;
    public HyperlinkEditorDialog(PageEditorView initEditorView, boolean isNewComponent){
        editorView=initEditorView;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        this.setTitle(props.getProperty(LABEL_HYPERLINK_EDIT_PROMPT));
	
	// SETUP THE PROMPT
	HyperlinkEditPromptLabel = new Label("URL");
	
	// INIT THE LANGUAGE CHOICES
	HyperlinkTextField=new TextField();
	okButton = new Button(props.getProperty(OK_BUTTON_TEXT));
	cancelButton = new Button(props.getProperty(CANCEL_BUTTON_TEXT));
	gridPane = new GridPane();
	gridPane.add(HyperlinkEditPromptLabel, 0, 0, 1, 1);
	gridPane.add(HyperlinkTextField, 1, 0, 2, 1);
	gridPane.add(okButton, 0, 1, 1, 1);
	gridPane.add(cancelButton, 1, 1, 1, 1);
	okButton.setOnAction(e -> {
	   
	    this.close();
	});
	cancelButton.setOnAction(e -> {
	   
	    this.close();
	});
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
	HyperlinkEditPromptLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
	okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
    }
           
}

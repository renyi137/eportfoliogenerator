/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.LanguagePropertyType;
import static epg.LanguagePropertyType.*;
import static epg.LanguagePropertyType.LABEL_HEADER_EDIT_PROMPT;
import static epg.LanguagePropertyType.LABEL_LIST_EDIT_PROMPT;
import static epg.LanguagePropertyType.OK_BUTTON_TEXT;
import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.*;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Item;
import epg.model.ListComponent;
import epg.model.Page;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author yiren
 */
public class ListComponentEditorDialog extends Stage{
    GridPane gridPane;
    PageEditorView editorView;
    Button okButton;
    Button cancelButton;
    Button addListButton;
    Button removeListButton;
    
    VBox listVBox;
    ScrollPane listScrollPane;
    FlowPane toolbar;
    ListComponent list;
    private final Label fontsFamilyLabel;
    private final ComboBox fontsFamilyComboBox;
    private final Label fontsSizeLabel;
    private final ComboBox fontsSizeComboBox;
    private final Button boldButton;
    private final Button italicButton;
    private final Button underlinedButton;
    private boolean isNewComponent;
    private boolean isBold;
    private boolean isItalic;
    private boolean isUnderlined;
    public ListComponentEditorDialog(PageEditorView initEditorView, boolean initisNewComponent){
        isBold=false;
        isItalic=false;
        isUnderlined=false;
        this.isNewComponent=initisNewComponent;
        editorView=initEditorView;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        this.setTitle(props.getProperty(LABEL_LIST_EDIT_PROMPT));
	list=new ListComponent();
	// SETUP THE PROMPT
	listVBox=new VBox();
        listScrollPane=new ScrollPane(listVBox);
        listScrollPane.setMinHeight(200);
        listScrollPane.setMinWidth(450);
	toolbar=new FlowPane();
	// INIT THE LANGUAGE CHOICES
        
	// SETUP THE PROMPT
	fontsFamilyLabel = new Label(props.getProperty(LABEL_FONTS_FAMILY.toString()));
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> fontsFamilyChoices = FXCollections.observableArrayList();
	fontsFamilyChoices.add(FONTS_FAMILY_1);
	fontsFamilyChoices.add(FONTS_FAMILY_2);
        fontsFamilyChoices.add(FONTS_FAMILY_3);
        fontsFamilyChoices.add(FONTS_FAMILY_4);
        fontsFamilyChoices.add(FONTS_FAMILY_5);
	fontsFamilyComboBox = new ComboBox(fontsFamilyChoices);
	fontsFamilyComboBox.getSelectionModel().select(FONTS_FAMILY_1);
	toolbar.getChildren().add(fontsFamilyLabel);
        toolbar.getChildren().add(fontsFamilyComboBox);
        
        fontsSizeLabel = new Label(props.getProperty(LABEL_FONTS_SIZE.toString()));
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> fontsSizeChoices = FXCollections.observableArrayList();
	fontsSizeChoices.add(FONTS_SIZE_1);
	fontsSizeChoices.add(FONTS_SIZE_2);
        fontsSizeChoices.add(FONTS_SIZE_3);
        fontsSizeChoices.add(FONTS_SIZE_4);
        fontsSizeChoices.add(FONTS_SIZE_5);
	fontsSizeComboBox = new ComboBox(fontsSizeChoices);
	fontsSizeComboBox.getSelectionModel().select(FONTS_SIZE_1);
	toolbar.getChildren().add(fontsSizeLabel);
        toolbar.getChildren().add(fontsSizeComboBox);
        boldButton=initChildButton(toolbar, ICON_BOLD, TOOLTIP_BOLD,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        italicButton=initChildButton(toolbar, ICON_ITALIC, TOOLTIP_ITALIC,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        underlinedButton=initChildButton(toolbar, ICON_UNDERLINED, TOOLTIP_UNDERLINED,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	addListButton=initChildButton(toolbar, ICON_ADD_ITEM,	TOOLTIP_ADD_LIST_ITEM,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton=initChildButton(toolbar, ICON_REMOVE_ITEM,	TOOLTIP_REMOVE_LIST_ITEM,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	okButton = new Button(props.getProperty(OK_BUTTON_TEXT));
	cancelButton = new Button(props.getProperty(CANCEL_BUTTON_TEXT));
        boldButton.setOnAction(e -> {
            if(isBold){
                isBold=false;
                boldButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED);
            }
            else{
                isBold=true;
                boldButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED);  
            }
        });
        italicButton.setOnAction(e -> {
            if(isItalic){
                isItalic=false;
                italicButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED);
            }
            else{
                isItalic=true;
                italicButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED);  
            }
        });
        underlinedButton.setOnAction(e -> {
            if(isUnderlined){
                isUnderlined=false;
                underlinedButton.getStyleClass().remove(CSS_CLASS_BUTTON_SELECTED);
            }
            else{
                isUnderlined=true;
                underlinedButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED);  
            }
        });
	gridPane = new GridPane();
	if(!this.isNewComponent){
            reloadListPane();
        }
	gridPane.add(toolbar, 0, 0, 1, 1);
        gridPane.add(listScrollPane, 0, 1, 1, 1);
	gridPane.add(okButton, 0, 2, 1, 1);
	gridPane.add(cancelButton, 2, 2, 1, 1);
        addListButton.setOnAction(e -> {
            TextField listItemTextField = new TextField();
            listItemTextField.setMinWidth(400);
            listVBox.getChildren().add(listItemTextField);
           
            removeListButton.setDisable(false);
        });
        removeListButton.setOnAction(e ->{
            int size=listVBox.getChildren().size();
            listVBox.getChildren().remove(size-1);
            if(listVBox.getChildren().isEmpty()){
                removeListButton.setDisable(true);
            }
            
        });
	okButton.setOnAction(e -> {
            Page editingPage=editorView.getPage();
            ListComponent listToAdd;
            if(isNewComponent){
                listToAdd=new ListComponent();
                listToAdd.setListItems(listVBox);
                listToAdd.setFontFamily(fontsFamilyComboBox.getSelectionModel().getSelectedItem().toString());
                listToAdd.setFontSize(fontsSizeComboBox.getSelectionModel().getSelectedItem().toString());
                listToAdd.setIsBold(isBold);
                listToAdd.setIsItalic(isItalic);
                listToAdd.setIsUnderlined(isUnderlined);
                editingPage.addListComponent(listToAdd);
                }
            else{
                listToAdd=(ListComponent)editingPage.getSelectedComponent();
                listToAdd.setListItems(listVBox);
                listToAdd.setFontFamily(fontsFamilyComboBox.getSelectionModel().getSelectedItem().toString());
                listToAdd.setFontSize(fontsSizeComboBox.getSelectionModel().getSelectedItem().toString());
                listToAdd.setIsBold(isBold);
                listToAdd.setIsItalic(isItalic);
                listToAdd.setIsUnderlined(isUnderlined);
            }
            editorView.reloadComponents();
	    
	    this.close();
	});
	cancelButton.setOnAction(e -> {
	   
	    this.close();
	});
	// SPECIFY STYLE CLASSES
        fontsFamilyLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        fontsSizeLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
      
        toolbar.getStyleClass().add(CSS_CLASS_PAGEEDITOR_HORIZONTAL_TOOLBAR_PANE);
	gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
	okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
        setScene(scene);
	
    }
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    public void reloadListPane() {
       listVBox.getChildren().clear();
       list=(ListComponent)editorView.getPage().getSelectedComponent();
	for (Item listItem : list.getList()) {
	    TextField listItemTextField = new TextField(listItem.getContent());
	    listItemTextField.setMinWidth(400);
	    listVBox.getChildren().add(listItemTextField);
	    
	    
	}
        if(list.isIsBold()){
            isBold=true;
            boldButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
        }
        if(list.isIsItalic()){
            isItalic=true;
            italicButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
        }
        if(list.isIsUnderlined()){
            isUnderlined=true;
            underlinedButton.getStyleClass().add(CSS_CLASS_BUTTON_SELECTED); 
        }
        fontsSizeComboBox.getSelectionModel().select(list.getFontSize());
        fontsFamilyComboBox.getSelectionModel().select(list.getFontFamily());
    }
            
}



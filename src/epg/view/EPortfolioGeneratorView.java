/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;
import epg.LanguagePropertyType;
import static epg.LanguagePropertyType.TOOLTIP_ADD_PAGE;
import static epg.LanguagePropertyType.TOOLTIP_EDIT;
import static epg.LanguagePropertyType.TOOLTIP_EXIT;
import static epg.LanguagePropertyType.TOOLTIP_EXPORT;
import static epg.LanguagePropertyType.TOOLTIP_LOAD;
import static epg.LanguagePropertyType.TOOLTIP_NEW;
import static epg.LanguagePropertyType.TOOLTIP_REMOVE_PAGE;
import static epg.LanguagePropertyType.TOOLTIP_SAVE;
import static epg.LanguagePropertyType.TOOLTIP_SAVEAS;
import static epg.LanguagePropertyType.TOOLTIP_VIEW;

import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_PANE;
import static epg.StartupConstants.CSS_CLASS_SLIDES_EDITOR_PANE;
import static epg.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_PANE;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE;
import static epg.StartupConstants.ICON_ADD_PAGE;
import static epg.StartupConstants.*;
import static epg.StartupConstants.ICON_EXIT;
import static epg.StartupConstants.ICON_EXPORT;
import static epg.StartupConstants.ICON_LOAD;
import static epg.StartupConstants.ICON_NEW;
import static epg.StartupConstants.ICON_REMOVE_PAGE;
import static epg.StartupConstants.ICON_SAVE;
import static epg.StartupConstants.ICON_SAVEAS;
import static epg.StartupConstants.ICON_VIEW;

import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.controller.FileController;
import epg.controller.ModeController;
import epg.controller.SiteController;
import epg.error.ErrorHandler;
import epg.file.EPortfolioExporter;
import epg.file.EPortfolioFileLoadManager;
import epg.file.EPortfolioFileManager;
import epg.model.Page;
import epg.model.Site;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;


/**
 *
 * @author yiren
 */
public class EPortfolioGeneratorView {
    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane epgPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newButton;
    Button loadButton;
    Button saveButton;
    Button saveAsButton;
    Button exportButton;
    Button exitButton;
    Label siteTitleLabel;
    TextField siteTitleTextField;
    
    BorderPane workspace;
    VBox leftContainer;
    FlowPane siteToolbar;
    Button addPageButton;
    Button removePageButton;
    
    ScrollPane pageSelectScrollPane;
    VBox pageSelectPane;
    
    FlowPane workspaceModeToolbar;
    Button pageEditorButton;
    Button siteViewerButton;
    Site site;
    
    EPortfolioFileManager fileManager;
    EPortfolioFileLoadManager fileManager2;
    EPortfolioExporter siteExporter;
    
    PageEditorView pageEditorView;
    
    SiteViewerView siteViewerView;
    private ErrorHandler errorHandler;
   
    private SiteController siteController;

    private ModeController modeController;
    private FileController fileController;
    public EPortfolioGeneratorView(EPortfolioFileManager initFileManager, EPortfolioFileLoadManager initFileManager2, EPortfolioExporter initSiteExporter) {
        fileManager = initFileManager;
        fileManager2 =initFileManager2;
	// AND THE SITE EXPORTER
	siteExporter = initSiteExporter;
	
	// MAKE THE DATA MANAGING MODEL
	site = new Site(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }
     public Site getSite() {
	return site;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    public void startUI(Stage initPrimaryStage, String windowTitle) {
       // THE TOOLBAR ALONG THE NORTH
	initFileToolbar();
        

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }
    private void initWorkspace(){
        workspace = new BorderPane();
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
        leftContainer=new VBox();
	siteToolbar = new FlowPane();
	addPageButton = this.initChildButton(siteToolbar,		ICON_ADD_PAGE,	    TOOLTIP_ADD_PAGE,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,  false);
	removePageButton = this.initChildButton(siteToolbar,	ICON_REMOVE_PAGE,  TOOLTIP_REMOVE_PAGE,   CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,  true);
	
	
	// AND THIS WILL GO IN THE CENTER
	pageSelectPane = new VBox();
	pageSelectScrollPane = new ScrollPane(pageSelectPane);
	pageSelectScrollPane.setFitToWidth(true);
	pageSelectScrollPane.setFitToHeight(true);
        pageSelectScrollPane.setMinHeight(590);

	workspaceModeToolbar=new FlowPane();
        
        pageEditorButton=initChildButton(workspaceModeToolbar, ICON_EDIT,	TOOLTIP_EDIT,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        siteViewerButton=initChildButton(workspaceModeToolbar, ICON_VIEW,	TOOLTIP_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	// NOW PUT THESE TWO IN THE WORKSPACE
        leftContainer.getChildren().add(siteToolbar);
        leftContainer.getChildren().add(pageSelectScrollPane);
        leftContainer.getChildren().add(workspaceModeToolbar);
        leftContainer.setMaxWidth(100);
	workspace.setLeft(leftContainer);
        pageEditorView=new PageEditorView(this);
	siteViewerView=new SiteViewerView(this);
        
	// SETUP ALL THE STYLE CLASSES
	workspace.getStyleClass().add(CSS_CLASS_WORKSPACE);
	siteToolbar.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_PANE);
	//pageSelectPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
	//pageSelectScrollPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
        workspaceModeToolbar.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_PANE);
    }
    private void initEventHandlers(){
        fileController = new FileController(this, fileManager, fileManager2, siteExporter);
        siteController=new SiteController(this);
        modeController=new ModeController(this);
        newButton.setOnAction(e -> {
	    fileController.handleNewRequest();
	});
        saveButton.setOnAction(e -> {
            fileController.handleSaveRequest();
        });
        saveAsButton.setOnAction(e -> {
            fileController.handleSaveAsRequest();
        });
        loadButton.setOnAction(e -> {
            fileController.handleLoadRequest();
        });
        exitButton.setOnAction(e ->{
            
        });
        exportButton.setOnAction(e ->{
            fileController.handleExportRequest(site.getTitle());
        });
        addPageButton.setOnAction(e -> {
	    siteController.processAddPageRequest();
            updateRemoveButton();
	});
        removePageButton.setOnAction(e -> {
	    siteController.processRemovePageRequest();
            updateRemoveButton();
	});
        pageEditorButton.setOnAction(e ->{
            modeController.handleEditModeRequest();
        });
        siteViewerButton.setOnAction(e ->{
            fileController.handleExportRequest("test");
            modeController.handleViewerModeRequest();
        });
        siteTitleTextField.textProperty().addListener(e -> {
	    site.setTitle(siteTitleTextField.getText());
	    
	});
    }
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
	fileToolbarPane.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_PANE);

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newButton = initChildButton(fileToolbarPane, ICON_NEW,	TOOLTIP_NEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadButton = initChildButton(fileToolbarPane, ICON_LOAD,	TOOLTIP_LOAD,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveButton = initChildButton(fileToolbarPane, ICON_SAVE,	TOOLTIP_SAVE,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	saveAsButton = initChildButton(fileToolbarPane, ICON_SAVEAS,	TOOLTIP_SAVEAS,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exportButton = initChildButton(fileToolbarPane, ICON_EXPORT,	TOOLTIP_EXPORT,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        siteTitleLabel=new Label("Title");
        siteTitleTextField=new TextField();
        fileToolbarPane.getChildren().add(siteTitleLabel);
        fileToolbarPane.getChildren().add(siteTitleTextField);
        siteTitleLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
    }
    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	epgPane = new BorderPane();
	epgPane.getStyleClass().add(CSS_CLASS_WORKSPACE);
	epgPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(epgPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    public void updateFileToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	epgPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveButton.setDisable(saved);
        saveAsButton.setDisable(saved);
	exportButton.setDisable(false);
	

    }
    public void updateRemoveButton(){
        if(site.getPages().isEmpty()||site.getSelectedPage()==null ){
            removePageButton.setDisable(true);
        }
        else{
            removePageButton.setDisable(false);
        }
    }
    public void reloadPageSelectPane() {
       siteTitleTextField.setText(site.getTitle());
       pageSelectPane.getChildren().clear();

	for (Page page : site.getPages()) {
	    Button pageButton = new Button(page.getTitle());
            pageButton.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
	    if (site.isSelectedPage(page))
		pageButton.getStyleClass().add(CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW);
	    else
		pageButton.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	   pageSelectPane.getChildren().add(pageButton);
	   pageButton.setOnAction(e -> {
		site.setSelectedPage(page);
		this.reloadPageSelectPane();
                updateRemoveButton();
                reloadPageEditorPane();
	    });
	}
        if(site.getPages().isEmpty()){
            workspace.setCenter(null);
        }
        fileController.markFileAsNotSaved();

    }
    public void markFileAsNotSaved(){
        fileController.markAsEdited();
    }
    
    public void reloadPageEditorPane(){
        pageEditorView.reloadPageEditorView();
        workspace.setCenter(pageEditorView);
    }
    public void reloadSiteViewerPane()
    {
        siteViewerView.reloadPage();
        workspace.setCenter(siteViewerView);
        
    }
}
    


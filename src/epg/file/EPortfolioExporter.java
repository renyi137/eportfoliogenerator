/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import epg.model.Component;
import epg.model.ImageComponent;
import epg.model.Page;
import epg.model.Site;
import epg.model.Slide;
import epg.model.SlideShowModel;
import epg.model.VideoComponent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 *
 * @author yiren
 */
public class EPortfolioExporter {
   // WE'LL USE THIS TO BUILD PATHS
    public static String SLASH = "/";
    public static String JSON_EXT = ".json";

    // HERE ARE THE DIRECTORIES WE CARE ABOUT
    public static String BASE_DIR = "./base/";
    public static String SITES_DIR = "./sites/";
    public static String CSS_DIR = "css/";
    public static String DATA_DIR = "data/";
    public static String EPORTFOLIO_DIR = DATA_DIR + "EPortfolio/";
    public static String ICONS_DIR = "icons/";
    public static String IMG_DIR = "img/";
    public static String JS_DIR = "js/";
    public static String VIDEO_DIR = "video/";

    // AND HERE ARE THE FILES WE CARE ABOUT
    public static String INDEX_FILE = "index.html";
    public static String STYLESHEET_SLIDESHOW = "slideshow_maker.css";
    public static String JS_SLIDESHOW = "SlideshowMaker.js";
    public static String DATA_FILE = "EPortfolioData.json";
    public static String INDEX_SLIDESHOW="slideshow.html";
    public static String STYLESHEET_File = "EPortfolioStyle.css";
    public static String JS_FILE = "EPortfolioMaker.js";
    

    public void exportSite(Site siteToExport, String name) throws IOException {	
	// THE SITE HOME PATH
	String homeSitePath = SITES_DIR + name + SLASH;
        File temp;
	// NOW MAKE THE SITE DIRECTORIES AND COPY OVER THE FILES
	// THAT ONLY NEED TO BE COPIED ONCE
	File siteDir = new File(homeSitePath);

	// FIRST DELETE THE OLD FILES IN CASE THINGS
	// LIKE THE PAGE FORMAT MAY HAVE CHANGED
	if (siteDir.exists())
	    deleteDir(siteDir);

	// NOW MAKE THE HOME DIR
	siteDir.mkdir();

	// MAKE THE CSS, DATA, IMG, AND JS DIRECTORIES
	new File(homeSitePath + CSS_DIR).mkdir();
	new File(homeSitePath + DATA_DIR).mkdir();
	new File(homeSitePath + ICONS_DIR).mkdir();
	new File(homeSitePath + IMG_DIR).mkdir();
	new File(homeSitePath + JS_DIR).mkdir();
        new File(homeSitePath + VIDEO_DIR).mkdir();

	// NOW COPY OVER THE HTML, CSS, ICON, AND JAVASCRIPT FILES
	//copyAllFiles(BASE_DIR, homeSitePath);
	copyAllFiles(BASE_DIR + CSS_DIR, homeSitePath + CSS_DIR);
	copyAllFiles(BASE_DIR + ICONS_DIR, homeSitePath + ICONS_DIR);
	copyAllFiles(BASE_DIR + JS_DIR, homeSitePath + JS_DIR);

	// NOW FOR THE TWO THINGS THAT WE HAVE TO COPY OVER EVERY TIME,
	// NAMELY, THE DATA FILE AND THE IMAGES
	// FIRST COPY THE DATA FILE
	Path dataSrcPath = new File(EPORTFOLIO_DIR + name + JSON_EXT).toPath();
	Path dataDestPath = new File(homeSitePath + DATA_DIR + DATA_FILE).toPath();

	Files.copy(dataSrcPath, dataDestPath);
        int i=0;
	// AND NOW ALL THE SLIDESHOW IMAGES
	for (Page p : siteToExport.getPages()) {
            int j=0;
             Path srcBannerImgPath = new File(p.getBannerImagePath() + p.getBannerImageFileName()).toPath();
             temp=new File(homeSitePath + IMG_DIR + p.getBannerImageFileName());
             Path destBannerImgPath = temp.toPath();
             if (!temp.exists()){
                        Files.copy(srcBannerImgPath, destBannerImgPath);
             }
             
                Path srcHtmlPath;
                System.out.println(p.getLayout());
            if(p.getLayout().equals("layout1")){
                srcHtmlPath=new File(BASE_DIR+"1.html").toPath();
            }
            else if(p.getLayout().equals("layout2")){
                srcHtmlPath=new File(BASE_DIR+"2.html").toPath();
            }
            else if(p.getLayout().equals("layout3")){
                srcHtmlPath=new File(BASE_DIR+"3.html").toPath();
            }
            else if(p.getLayout().equals("layout4")){
                srcHtmlPath=new File(BASE_DIR+"4.html").toPath();
            }
            else{
                srcHtmlPath=new File(BASE_DIR+"5.html").toPath();
            }
            Path destHtmlPath;
            if(i==0){
                destHtmlPath=new File(homeSitePath+"index.html").toPath();
                
            }
            else{
                destHtmlPath=new File(homeSitePath+p.getTitle()+".html").toPath();}
            Files.copy(srcHtmlPath, destHtmlPath);
            for(Component c : p.getComponents()){
                if(c instanceof ImageComponent){
                    ImageComponent ic=(ImageComponent)c;
                    Path srcImgPath = new File(ic.getPath() + ic.getFileName()).toPath();
                    temp=new File(homeSitePath + IMG_DIR + ic.getFileName());
                    Path destImgPath = temp.toPath();
                    if(!temp.exists()){
                        Files.copy(srcImgPath, destImgPath);
                    }
                }
                if(c instanceof VideoComponent){
                    VideoComponent vc=(VideoComponent)c;
                    Path srcVideoPath = new File(vc.getPath() + vc.getFileName()).toPath();
                    temp = new File(homeSitePath + VIDEO_DIR + vc.getFileName());
                    Path destVideoPath=temp.toPath();
                    if(!temp.exists()){
                        Files.copy(srcVideoPath, destVideoPath);
                    }
                }
                if(c instanceof SlideShowModel){
                    SlideShowModel slideShow=(SlideShowModel)c;
                    Path srcSlideShowHtmlPath=new File(BASE_DIR+INDEX_SLIDESHOW).toPath();
                    Path destSlideShowHtmlPath=new File(homeSitePath+i+"_"+j+".html").toPath();
                    Files.copy(srcSlideShowHtmlPath, destSlideShowHtmlPath);
                    for (Slide s : slideShow.getSlides()) {
                        Path srcImgPath = new File(s.getImagePath() + SLASH + s.getImageFileName()).toPath();
                        temp=new File(homeSitePath + IMG_DIR + s.getImageFileName());
                        Path destImgPath = temp.toPath();
                        if(!temp.exists()){
                        Files.copy(srcImgPath, destImgPath);
                        }
                    }
                    j++;
                }
	    
            }
            i++;
	}
    }
    
    public void deleteDir(File dir) {
	File[] files = dir.listFiles();
	for (File f : files) {
	    if (f.isDirectory()) {
		deleteDir(f);
		f.delete();
	    }
	    else
		f.delete();
	}
	dir.delete();
    }

    public void copyAllFiles(String sourceFile, String destinationDir) throws IOException {
	File srcDir = new File(sourceFile);
	File[] files = srcDir.listFiles();
	for (File f : files) {
	    Path srcPath = f.toPath();
	    Path newPath = new File(destinationDir).toPath();
	    if (!f.isDirectory()) {
		Files.copy(srcPath, newPath.resolve(srcPath.getFileName()));
	    }
	}
    }
}



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import static epg.StartupConstants.PATH_EPORTFOLIO;
import epg.model.Component;
import epg.model.HeaderComponent;
import epg.model.ImageComponent;
import epg.model.Item;
import epg.model.ListComponent;
import epg.model.Page;
import epg.model.ParagraphComponent;
import epg.model.Site;
import epg.model.Slide;
import epg.model.SlideShowModel;
import epg.model.VideoComponent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author yiren
 */
public class EPortfolioFileManager {
     // JSON FILE READING AND WRITING CONSTANTS

    public static String JSON_TITLE = "title";
    public static String JSON_PAGES = "pages";
    public static String JSON_PAGE_TITLE="page_title";
    public static String JSON_STUDENT_NAME="student_name";
    public static String JSON_FOOTER="footer";
    public static String JSON_FONT_FAMILY="font_family";
    public static String JSON_FONT_SIZE="font_size";
    public static String JSON_IS_BOLD="is_bold";
    public static String JSON_IS_ITALIC="is_italic";
    public static String JSON_IS_UNDERLINED="is_underlined";
    public static String JSON_LAYOUT="layout";
    public static String JSON_COLOR_TEMPLATE="color_template";
    public static String JSON_HEADER_COMPONENTS="header_components";
    public static String JSON_LIST_COMPONENTS="list_components";
    public static String JSON_PARAGRAPH_COMPONENTS="paragraph_components";
    public static String JSON_IMAGE_COMPONENTS="image_components";
    public static String JSON_VIDEO_COMPONENTS="video_components";
    public static String JSON_SLIDESHOW_COMPONENTS="slideshow_components";
            
    public static String JSON_BANNER_IMAGE_FILE_NAME = "banner_image_file_name";
    public static String JSON_BANNER_IMAGE_PATH = "banner_image_path";
    
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    public static String JSON_CONTENT="content";
    public static String JSON_LIST="list";
    public static String JSON_SLIDES="slides";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_WIDTH="width";
    public static String JSON_HEIGHT="height";
    public static String JSON_FLOATING="floating";
    public static String JSON_ORDER_COMPONENTS="order_components";
      
    /**
     * This method saves all the data associated with a slide show to a JSON
     * file.
     *
     * @param slideShowToSave The course whose data we are saving.
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    public void saveEPortfolio(Site siteToSave, String fileName) throws IOException {
        
	StringWriter sw = new StringWriter();
        siteToSave.setIsSaved(true);
	// BUILD THE SLIDES ARRAY
	JsonArray pagesJsonArray = makePagesJsonArray(siteToSave.getPages());

	// NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
	JsonObject EPortfolioJsonObject = Json.createObjectBuilder()
		.add(JSON_TITLE, fileName)
                .add("isSaved", siteToSave.isIsSaved())
		.add(JSON_PAGES, pagesJsonArray)
		.build();

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);

	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(EPortfolioJsonObject);
	jsonWriter.close();

	// INIT THE WRITER
	
	String jsonFilePath = PATH_EPORTFOLIO + fileName + JSON_EXT;
	OutputStream os = new FileOutputStream(jsonFilePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(EPortfolioJsonObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFilePath);
	pw.write(prettyPrinted);
	pw.close();
	System.out.println(prettyPrinted);
    }

    /**
     * This method loads the contents of a JSON file representing a slide show
     * into a SlideSShowModel objecct.
     *
     * @param slideShowToLoad The slide show to load
     * @param jsonFilePath The JSON file to load.
     * @throws IOException
     */
   
    
    private JsonArray makePagesJsonArray(List<Page> pages) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Page page : pages) {
            page.makeOrderOfComponents();
	    JsonObject jso = makePageJsonObject(page);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }

    private JsonObject makePageJsonObject(Page page) {
        JsonArray headerComponentsJsonArray = makeHeaderComponentsJsonArray(page.getComponents());
        JsonArray listComponentsJsonArray = makeListComponentsJsonArray(page.getComponents());
        JsonArray paragraphComponentsJsonArray = makeParagraphComponentsJsonArray(page.getComponents());
        JsonArray imageComponentsJsonArray = makeImageComponentsJsonArray(page.getComponents());
        JsonArray videoComponentsJsonArray = makeVideoComponentsJsonArray(page.getComponents());
        JsonArray slideshowComponentsJsonArray = makeSlideshowComponentsJsonArray(page.getComponents());
        JsonArray orderOfComponentsJsonArray = makeOrderOfComponentsJsonArray(page.getOrderOfComponents());
	JsonObject jso = Json.createObjectBuilder()
                .add(JSON_PAGE_TITLE, page.getTitle())
                .add(JSON_STUDENT_NAME, page.getStudentName())
                .add(JSON_FOOTER,page.getFooter())
		.add(JSON_BANNER_IMAGE_FILE_NAME, page.getBannerImageFileName())
		.add(JSON_BANNER_IMAGE_PATH, page.getBannerImagePath())
                .add(JSON_FONT_FAMILY, page.getFontFamily())
                .add(JSON_FONT_SIZE, page.getFontSize())
                .add(JSON_IS_BOLD, page.isIsBold())
                .add(JSON_IS_ITALIC,page.isIsItalic())
                .add(JSON_IS_UNDERLINED, page.isIsUnderlined())
                .add(JSON_LAYOUT,page.getLayout())
                .add(JSON_COLOR_TEMPLATE,page.getColorTemplate())
                .add(JSON_HEADER_COMPONENTS, headerComponentsJsonArray)
                .add(JSON_LIST_COMPONENTS, listComponentsJsonArray)
                .add(JSON_PARAGRAPH_COMPONENTS, paragraphComponentsJsonArray)
                .add(JSON_VIDEO_COMPONENTS, videoComponentsJsonArray)
                .add(JSON_IMAGE_COMPONENTS, imageComponentsJsonArray)
                .add(JSON_SLIDESHOW_COMPONENTS, slideshowComponentsJsonArray)
                .add(JSON_ORDER_COMPONENTS, orderOfComponentsJsonArray)
		.build();
	return jso;
    }
    private JsonArray makeHeaderComponentsJsonArray(ObservableList<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Component component : components) {
            if(component instanceof HeaderComponent){
                HeaderComponent headerComponent=(HeaderComponent)component;
                JsonObject jso = makeHeaderComponentJsonObject(headerComponent);
                jsb.add(jso);
            }
	}
	JsonArray jA = jsb.build();
	return jA;
        
    }
    private JsonObject makeHeaderComponentJsonObject(HeaderComponent component){
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_CONTENT, component.getContent())
                .build();
        return jso;
     
    }
    private JsonArray makeListComponentsJsonArray(ObservableList<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Component component : components) {
            if(component instanceof ListComponent){
                ListComponent listComponent=(ListComponent)component;
                JsonObject jso = makeListComponentJsonObject(listComponent);
                jsb.add(jso);
            }
	}
	JsonArray jA = jsb.build();
	return jA;
        
    }
    private JsonObject makeListComponentJsonObject(ListComponent component){
        JsonArray listJsonArray = makeListJsonArray(component.getList());
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_LIST, listJsonArray)
                .add(JSON_FONT_FAMILY, component.getFontFamily())
                .add(JSON_FONT_SIZE, component.getFontSize())
                .add(JSON_IS_BOLD, component.isIsBold())
                .add(JSON_IS_ITALIC,component.isIsItalic())
                .add(JSON_IS_UNDERLINED, component.isIsUnderlined())
                .build();
        return jso;
     
    }
    private JsonArray makeListJsonArray(ObservableList<Item> list) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Item item : list) {
            
                
                JsonObject jso = makeItemJsonObject(item);
                jsb.add(jso);
            
	}
	JsonArray jA = jsb.build();
	return jA;
    }
    public JsonObject makeItemJsonObject(Item item){
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_CONTENT, item.getContent())
                .build();
        return jso;
    }
     private JsonArray makeParagraphComponentsJsonArray(ObservableList<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Component component : components) {
            if(component instanceof ParagraphComponent){
                ParagraphComponent paragraphComponent=(ParagraphComponent)component;
                JsonObject jso = makeParagraphComponentJsonObject(paragraphComponent);
                jsb.add(jso);
            }
	}
	JsonArray jA = jsb.build();
	return jA;
        
    }
    private JsonObject makeParagraphComponentJsonObject(ParagraphComponent component){
        
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_CONTENT, component.getContent())
                .add(JSON_FONT_FAMILY, component.getFontFamily())
                .add(JSON_FONT_SIZE, component.getFontSize())
                .add(JSON_IS_BOLD, component.isIsBold())
                .add(JSON_IS_ITALIC,component.isIsItalic())
                .add(JSON_IS_UNDERLINED, component.isIsUnderlined())
                .build();
        return jso;
     
    }
    private JsonArray makeSlideshowComponentsJsonArray(ObservableList<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Component component : components) {
            if(component instanceof SlideShowModel){
                SlideShowModel slideshow=(SlideShowModel)component;
                JsonObject jso = makeSlideshowComponentJsonObject(slideshow);
                jsb.add(jso);
            }
	}
	JsonArray jA = jsb.build();
	return jA;
        
    }
    private JsonObject makeSlideshowComponentJsonObject(SlideShowModel component){
        
        JsonArray slidesJsonArray = makeSlidesJsonArray(component.getSlides());

	// NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
	JsonObject jso = Json.createObjectBuilder()
		.add(JSON_TITLE, component.getTitle())
		.add(JSON_SLIDES, slidesJsonArray)
		.build();
        return jso;
     
    }
    private JsonArray makeSlidesJsonArray(List<Slide> slides) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Slide slide : slides) {
	    JsonObject jso = makeSlideJsonObject(slide);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }

    private JsonObject makeSlideJsonObject(Slide slide) {
	JsonObject jso = Json.createObjectBuilder()
		.add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
		.add(JSON_IMAGE_PATH, slide.getImagePath())
		.add(JSON_CAPTION, slide.getCaption())
		.build();
	return jso;
    }
     private JsonArray makeImageComponentsJsonArray(ObservableList<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Component component : components) {
            if(component instanceof ImageComponent){
                ImageComponent imageComponent=(ImageComponent)component;
                JsonObject jso = makeImageComponentJsonObject(imageComponent);
                jsb.add(jso);
            }
	}
	JsonArray jA = jsb.build();
	return jA;
        
    }
    private JsonObject makeImageComponentJsonObject(ImageComponent component){
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_IMAGE_PATH, component.getPath())
                .add(JSON_IMAGE_FILE_NAME, component.getFileName())
                .add(JSON_WIDTH, component.getWidth())
                .add(JSON_HEIGHT, component.getHeight())
                .add(JSON_CAPTION,component.getCaption())
                .add(JSON_FLOATING, component.getFloating())
                .build();
        return jso;
     
    }
    private JsonArray makeVideoComponentsJsonArray(ObservableList<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Component component : components) {
            if(component instanceof VideoComponent){
                VideoComponent videoComponent=(VideoComponent)component;
                JsonObject jso = makeVideoComponentJsonObject(videoComponent);
                jsb.add(jso);
            }
	}
	JsonArray jA = jsb.build();
	return jA;
        
    }
    private JsonObject makeVideoComponentJsonObject(VideoComponent component){
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_IMAGE_PATH, component.getPath())
                .add(JSON_IMAGE_FILE_NAME, component.getFileName())
                .add(JSON_WIDTH, component.getWidth())
                .add(JSON_HEIGHT, component.getHeight())
                .add(JSON_CAPTION,component.getCaption())
 
                .build();
        return jso;
     
    }
    private JsonArray makeOrderOfComponentsJsonArray(ObservableList<String> order) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (String i : order) {

                JsonObject jso  = Json.createObjectBuilder()
                .add("order",i)
                .build();
                jsb.add(jso);
            
	}
	JsonArray jA = jsb.build();
	return jA;
        
    }
   
    
   
    
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import static epg.file.EPortfolioFileManager.JSON_BANNER_IMAGE_FILE_NAME;
import static epg.file.EPortfolioFileManager.JSON_BANNER_IMAGE_PATH;
import static epg.file.EPortfolioFileManager.JSON_COLOR_TEMPLATE;
import static epg.file.EPortfolioFileManager.JSON_FONT_FAMILY;
import static epg.file.EPortfolioFileManager.JSON_FONT_SIZE;
import static epg.file.EPortfolioFileManager.JSON_FOOTER;
import static epg.file.EPortfolioFileManager.JSON_IS_BOLD;
import static epg.file.EPortfolioFileManager.JSON_IS_ITALIC;
import static epg.file.EPortfolioFileManager.JSON_IS_UNDERLINED;
import static epg.file.EPortfolioFileManager.JSON_LAYOUT;
import static epg.file.EPortfolioFileManager.JSON_PAGES;
import static epg.file.EPortfolioFileManager.JSON_STUDENT_NAME;
import static epg.file.EPortfolioFileManager.JSON_TITLE;
import epg.model.Component;
import epg.model.HeaderComponent;
import epg.model.Page;
import epg.model.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Stack;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

/**
 *
 * @author yiren
 */
public class EPortfolioFileLoadManager {
     public static String JSON_TITLE = "title";
    public static String JSON_PAGES = "pages";
    public static String JSON_PAGE_TITLE="page_title";
    public static String JSON_STUDENT_NAME="student_name";
    public static String JSON_FOOTER="footer";
    public static String JSON_FONT_FAMILY="font_family";
    public static String JSON_FONT_SIZE="font_size";
    public static String JSON_IS_BOLD="is_bold";
    public static String JSON_IS_ITALIC="is_italic";
    public static String JSON_IS_UNDERLINED="is_underlined";
    public static String JSON_LAYOUT="layout";
    public static String JSON_COLOR_TEMPLATE="color_template";
    public static String JSON_HEADER_COMPONENTS="header_components";
    public static String JSON_LIST_COMPONENTS="list_components";
    public static String JSON_PARAGRAPH_COMPONENTS="paragraph_components";
    public static String JSON_IMAGE_COMPONENTS="image_components";
    public static String JSON_VIDEO_COMPONENTS="video_components";
    public static String JSON_SLIDESHOW_COMPONENTS="slideshow_components";
            
    public static String JSON_BANNER_IMAGE_FILE_NAME = "banner_image_file_name";
    public static String JSON_BANNER_IMAGE_PATH = "banner_image_path";
    
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    public static String JSON_CONTENT="content";
    public static String JSON_LIST="list";
    public static String JSON_SLIDES="slides";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_WIDTH="width";
    public static String JSON_HEIGHT="height";
    public static String JSON_FLOATING="floating";
    public static String JSON_ORDER_COMPONENTS="order_components";
     public void loadSite(Site siteToLoad, String jsonFilePath) throws IOException {
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(jsonFilePath);

	// NOW LOAD THE COURSE
	siteToLoad.reset();
	siteToLoad.setTitle(json.getString(JSON_TITLE));
        siteToLoad.setIsSaved(json.getBoolean("isSaved"));
	JsonArray jsonPagesArray = json.getJsonArray(JSON_PAGES);
	for (int i = 0; i < jsonPagesArray.size(); i++) {
	    JsonObject pageJso = jsonPagesArray.getJsonObject(i);
            boolean isBold=pageJso.getBoolean(JSON_IS_BOLD);
            boolean isItalic=pageJso.getBoolean(JSON_IS_ITALIC);
            boolean isUnderlined=pageJso.getBoolean(JSON_IS_UNDERLINED);;
	    Page pageToAdd=new Page(pageJso.getString(JSON_PAGE_TITLE), pageJso.getString(JSON_STUDENT_NAME), pageJso.getString(JSON_FOOTER), pageJso.getString(JSON_LAYOUT),
            pageJso.getString(JSON_COLOR_TEMPLATE), pageJso.getString(JSON_BANNER_IMAGE_PATH), pageJso.getString(JSON_BANNER_IMAGE_FILE_NAME), isBold,
            isItalic,isUnderlined, pageJso.getString(JSON_FONT_FAMILY), pageJso.getString(JSON_FONT_SIZE));
            pageToAdd.setComponents(loadComponents(pageJso));
            siteToLoad.getPages().add(pageToAdd);
	}
    }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
	JsonObject json = loadJSONFile(jsonFilePath);
	ArrayList<String> items = new ArrayList();
	JsonArray jsonArray = json.getJsonArray(arrayName);
	for (JsonValue jsV : jsonArray) {
	    items.add(jsV.toString());
	}
	return items;
    }
    public ObservableList<Component> loadComponents(JsonObject pageJso){
        JsonArray jsonOrderArray = pageJso.getJsonArray(JSON_ORDER_COMPONENTS);
        JsonArray jsonHeaderComponentArray = pageJso.getJsonArray(JSON_HEADER_COMPONENTS);
        JsonArray jsonListComponentArray = pageJso.getJsonArray(JSON_LIST_COMPONENTS);
        JsonArray jsonParagraphComponentArray = pageJso.getJsonArray(JSON_PARAGRAPH_COMPONENTS);
        JsonArray jsonImageComponentArray = pageJso.getJsonArray(JSON_IMAGE_COMPONENTS);
        JsonArray jsonVideoComponentArray = pageJso.getJsonArray(JSON_VIDEO_COMPONENTS);
        JsonArray jsonSlideshowComponentArray = pageJso.getJsonArray(JSON_SLIDESHOW_COMPONENTS);
        Stack headerComponentStack=makeHeaderComponentStack(jsonHeaderComponentArray);
        Stack listComponentStack=makeListComponentStack(jsonListComponentArray);
        Stack paragraphComponentStack=makeParagraphComponentStack(jsonParagraphComponentArray);
        Stack imageComponentStack=makeImageComponentStack(jsonImageComponentArray);
        Stack videoComponentStack=makeVideoComponentStack(jsonVideoComponentArray);
        Stack slideshowComponentStack=makeSlideshowComponentStack(jsonSlideshowComponentArray);
        ObservableList<Component> components=FXCollections.observableArrayList();
        for (int i = 0; i < jsonOrderArray.size(); i++) {
	    JsonObject orderJso = jsonOrderArray.getJsonObject(i);
            String order=orderJso.getString("order");
            if(order.equals("h")){
                components.add((HeaderComponent)headerComponentStack.pop());
            }
            if(order.equals("l")){
                components.add((ListComponent)listComponentStack.pop());
            }
            if(order.equals("p")){
                components.add((ParagraphComponent)paragraphComponentStack.pop());
            }
            if(order.equals("i")){
                components.add((ImageComponent)imageComponentStack.pop());
            }
            if(order.equals("v")){
                components.add((VideoComponent)videoComponentStack.pop());
            }
            if(order.equals("s")){
                components.add((SlideShowModel)slideshowComponentStack.pop());
            }
	    
	}
        return components;
    }
    public Stack makeHeaderComponentStack(JsonArray jsonHeaderComponentArray){
        Stack stack=new Stack();
        for (int i = 0; i < jsonHeaderComponentArray.size(); i++) {
            JsonObject jso = jsonHeaderComponentArray.getJsonObject(i);
            stack.push(new HeaderComponent(jso.getString(JSON_CONTENT)));
        }
        return stack;
    }
    public Stack makeListComponentStack(JsonArray jsonListComponentArray){
        Stack stack=new Stack();
        for (int i = 0; i < jsonListComponentArray.size(); i++) {
            JsonObject jso = jsonListComponentArray.getJsonObject(i);
            ListComponent listComponent=new ListComponent();
            listComponent.setList(makeList(jso.getJsonArray(JSON_LIST)));
            listComponent.setFontFamily(jso.getString(JSON_FONT_FAMILY));
            listComponent.setFontSize(jso.getString(JSON_FONT_SIZE));
            listComponent.setIsBold(jso.getBoolean(JSON_IS_BOLD));
            listComponent.setIsItalic(jso.getBoolean(JSON_IS_ITALIC));
            listComponent.setIsUnderlined(jso.getBoolean(JSON_IS_UNDERLINED));
            stack.push(listComponent);
        }
        return stack;
    }
    public ObservableList<Item> makeList(JsonArray jsonListArray){
        ObservableList<Item> list=FXCollections.observableArrayList();
        for (int i = 0; i < jsonListArray.size(); i++) {
            JsonObject jso = jsonListArray.getJsonObject(i);
            list.add(new Item(jso.getString(JSON_CONTENT)));
            
        }
        return list;
    }
    public Stack makeParagraphComponentStack(JsonArray jsonParagraphComponentArray){
        Stack stack=new Stack();
        for (int i = 0; i < jsonParagraphComponentArray.size(); i++) {
            JsonObject jso = jsonParagraphComponentArray.getJsonObject(i);
            ParagraphComponent component=new ParagraphComponent();
            component.setContent(jso.getString(JSON_CONTENT));
            component.setFontFamily(jso.getString(JSON_FONT_FAMILY));
            component.setFontSize(jso.getString(JSON_FONT_SIZE));
            component.setIsBold(jso.getBoolean(JSON_IS_BOLD));
            component.setIsItalic(jso.getBoolean(JSON_IS_ITALIC));
            component.setIsUnderlined(jso.getBoolean(JSON_IS_UNDERLINED));
            stack.push(component);
        }
        return stack;
    }
    public Stack makeImageComponentStack(JsonArray jsonImageComponentArray){
        Stack stack=new Stack();
        for (int i = 0; i < jsonImageComponentArray.size(); i++) {
            JsonObject jso = jsonImageComponentArray.getJsonObject(i);
            ImageComponent component=new ImageComponent();
            component.setFileName(jso.getString(JSON_IMAGE_FILE_NAME));
            component.setCaption(jso.getString(JSON_CAPTION));
            component.setPath(jso.getString(JSON_IMAGE_PATH));
            component.setHeight(jso.getString(JSON_HEIGHT));
            component.setWidth(jso.getString(JSON_WIDTH));
            component.setFloating(jso.getString(JSON_FLOATING));
            stack.push(component);
        }
        return stack;
    }
    public Stack makeVideoComponentStack(JsonArray jsonVideoComponentArray){
        Stack stack=new Stack();
        for (int i = 0; i < jsonVideoComponentArray.size(); i++) {
            JsonObject jso = jsonVideoComponentArray.getJsonObject(i);
            VideoComponent component=new VideoComponent();
            component.setFileName(jso.getString(JSON_IMAGE_FILE_NAME));
            component.setCaption(jso.getString(JSON_CAPTION));
            component.setPath(jso.getString(JSON_IMAGE_PATH));
            component.setHeight(jso.getString(JSON_HEIGHT));
            component.setWidth(jso.getString(JSON_WIDTH));
            
            stack.push(component);
        }
        return stack;
    }
    public Stack makeSlideshowComponentStack(JsonArray jsonSlideshowComponentArray){
        Stack stack=new Stack();
        for (int i = 0; i < jsonSlideshowComponentArray.size(); i++) {
            JsonObject jso = jsonSlideshowComponentArray.getJsonObject(i);
            SlideShowModel slideshow=new SlideShowModel();
            
            slideshow.setTitle(jso.getString(JSON_TITLE));
            JsonArray jsonSlidesArray = jso.getJsonArray(JSON_SLIDES);
            for (int j = 0; j < jsonSlidesArray.size(); j++) {
	    JsonObject slideJso = jsonSlidesArray.getJsonObject(j);
	    slideshow.addSlide(slideJso.getString(JSON_IMAGE_FILE_NAME),
		    slideJso.getString(JSON_IMAGE_PATH),
		    slideJso.getString(JSON_CAPTION));
            }
            stack.push(slideshow);
        }
            
            
      
        return stack;
    }
    
    
}

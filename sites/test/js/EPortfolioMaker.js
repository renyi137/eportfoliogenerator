/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var currentPage;
var navItem;
var pageTitle;
var studentName;
var footer;
var bannerImageFileName;
var fontFamily;
var fontSize;
var isBold;
var isItalic;
var isUnderlined;
var layout;
var colorTemplate;
var headerComponents;
var listComponents;
var paragraphComponents;
var imageComponents;
var videoComponents;
var slideshowComponents;
var order;
var VIDEO_PATH;
// TIMER FOR PLAYING SLIDESHOW
var timer;

function HeaderComponent(initContent){
    this.content=initContent;
}

function ListComponent(initList, initFontFamily, initFontSize, initIsBold, initIsItalic, initIsUnderlined){
    this.list=initList;
    this.fontFamily=initFontFamily;
    this.fontSize=initFontSize;
    this.isBold=initIsBold;
    this.isItalic=initIsItalic;
    this.isUnderlined=initIsUnderlined;
    
}
function ParagraphComponent(initContent, initFontFamily, initFontSize, initIsBold, initIsItalic, initIsUnderlined){
    this.content=initContent;
    this.fontFamily=initFontFamily;
    this.fontSize=initFontSize;
    this.isBold=initIsBold;
    this.isItalic=initIsItalic;
    this.isUnderlined=initIsUnderlined;   
}
function ImageComponent(initImageFileName, initWidth, initHeight,initCaption, initFloating){
    this.imageFileName=initImageFileName;
    this.width=initWidth;
    this.height=initHeight;
    this.caption=initCaption;
    this.floating=initFloating;  
}
function VideoComponent(initImageFileName, initWidth, initHeight,initCaption){
    this.imageFileName=initImageFileName;
    this.width=initWidth;
    this.height=initHeight;
    this.caption=initCaption;
}
function SlideshowComponent(initTitle, initSlides){
    this.title=initTitle;
    this.slides=initSlides;
}
function Slide(initImgFile, initCaption) {
    this.imgFile = initImgFile;
    this.caption = initCaption;
}



function initEPortfolio() {
    
   
    

    IMG_PATH = "./img/";
    ICON_PATH = "./icons/";
    VIDEO_PATH="./video/";
    IMG_WIDTH = 1000;
    SCALED_IMAGE_HEIGHT = 500.0;
    FADE_TIME = 1000;
    SLIDESHOW_SLEEP_TIME = 3000;
   
    navItem=new Array();
    headerComponents=new Array();
    listComponents=new Array();
    paragraphComponents=new Array();
    imageComponents=new Array();
    videoComponents=new Array();
    slideshowComponents=new Array();
    order=new Array();
    var EPortfolioDataFile = "./data/EPortfolioData.json";
    loadData(EPortfolioDataFile);
    timer = null;
}

function initPage() {
    $("title").text(pageTitle);
    $("body").addClass(colorTemplate); 
    initFont("#banner_text",fontFamily,fontSize, isBold, isItalic, isUnderlined);
    initFont("#footer",fontFamily, fontSize, isBold, isItalic, isUnderlined);
    initNavBar();
    initBanner();
    initComponents();
    $("#footer").text(footer);
}
function initComponents(){
    var h=0;
    var l=0;
    var p=0;
    var i=0;
    var v=0;
    var s=0;
    for(var j=0;j<order.length; j++){
        
        if(order[j]==="h"){
            addHeader(h);
            h++;
        }
        if(order[j]==="l"){
            addList(l);
            l++;
        }
        if(order[j]==="p"){
            addParagraph(p);
            p++;
        }
        if(order[j]==="i"){
            addImage(i);
            i++;
        }
        if(order[j]==="v"){
            addVideo(v);
            v++;
        }
        if(order[j]==="s"){
            addSlideshow(s);
            s++;
        }
    }
function addSlideshow(index){
    var slideshowToAdd=slideshowComponents[index];
    var slideshowId=currentPage+"_"+index;
    
    title=slideshowToAdd.title;
    slides=slideshowToAdd.slides;
    $("#components_container").append("<div class=slideshow_component><iframe src="+slideshowId+".html></div>");
}

function addVideo(index){
    var videoToAdd=videoComponents[index];
    var videoId="video"+index;
    var type="video/"+videoToAdd.imageFileName.substr(videoToAdd.imageFileName.lastIndexOf(".")+1);
    $("#components_container").append("<div class=video_component><video controls id="+videoId+"><source src="+VIDEO_PATH+videoToAdd.imageFileName+" type="+type+
            "></video><div class='caption'>"+videoToAdd.caption+"</div></div>"); 
    $("#"+videoId).css({"width": videoToAdd.width,"height": videoToAdd.height});  
}
//<div class="component" id="video_container">
                //<video controls>
                //<source src="./video/mov.mp4" type="video/mp4">
                //</video>
            //</div>
    
}
function addImage(index){
    var imageToAdd=imageComponents[index];
    var imageId="image"+index;
    var imageComponentId="imageComponent"+index;
    $("#components_container").append("<div class=image_component id="+imageComponentId+"><img id="+imageId+" src="+IMG_PATH+imageToAdd.imageFileName+"/><div class='caption'>"+imageToAdd.caption+"</div></div>"); 
    $("#"+imageId).css({"width": imageToAdd.width,"height": imageToAdd.height});
    $("#"+imageComponentId).css({"float":imageToAdd.floating});
    
}
//<div class="component" id="image_container">
               // <img id="image1" src="./img/ArchesUtah.jpg" />
            //</div>
function addHeader(index){
    var headerToAdd=headerComponents[index];
    $("#components_container").append("<div class=header_component id=header"+index+"><h1>"+headerToAdd.content+"</h1></div>");   
}
function addList(index){
    var listToAdd=listComponents[index];
    var listId="list"+index;
    $("#components_container").append("<div class=list_component ><ul id="+listId+" style=list-style-type:disc></ul></div>"); 
    for(var i=0;i<listToAdd.list.length; i++){
        $("#"+listId).append("<li>"+listToAdd.list[i]+"</li>");
    }
    initFont("#"+listId,listToAdd.fontFamily, listToAdd.fontSize, listToAdd.isBold, listToAdd.isItalic, listToAdd.isUnderlined);
}
function addParagraph(index){
    var paragraphToAdd=paragraphComponents[index];
    var paragraphId="paragraph"+index;
    $("#components_container").append("<div class=paragph_component ><p id="+paragraphId+">"+paragraphToAdd.content+"</p></div>");
    initFont("#"+paragraphId,paragraphToAdd.fontFamily, paragraphToAdd.fontSize, paragraphToAdd.isBold, paragraphToAdd.isItalic, paragraphToAdd.isUnderlined);
}


function initFont(idToInit, rawfontFamily, fontSize, isBold, isItalic, isUnderlined){
    var fontFamily;
    if(rawfontFamily==="Montserrat"){
        fontFamily="'Montserrat', sans-serif";
    }
    if(rawfontFamily==="Open Sans Condensed"){
        fontFamily="'Open Sans Condensed', sans-serif";
    }
    if(rawfontFamily==="Poiret One"){
        fontFamily="'Poiret One', cursive";
    }
    if(rawfontFamily==="Titillium Web"){
        fontFamily="'Titillium Web', sans-serif";
    }
    if(rawfontFamily==="Dosis"){
        fontFamily="'Dosis', sans-serif";
    }
    if(isBold){
        $(idToInit).css({"font-weight": "bold","font-size": fontSize,"font-family":fontFamily});
    }
    if(isItalic){
        $(idToInit).css({"font-style":"italic","font-size": fontSize,"font-family":fontFamily});
    }
    if(isUnderlined){
        $(idToInit).css({"text-decoration":"underline","font-size": fontSize,"font-family":fontFamily});
    }
    
}

function initNavBar(){
    for(var i=0; i<navItem.length; i++){
        var pageHref;
        var className;
        if(i===0){
            pageHref="index.html";
        }
        else{
            pageHref=navItem[i]+".html";
        }
        if(navItem[i]===pageTitle){
            className="open_nav";
        }
        else{
            className="nav";
        }
        $("#navbar_container").append("<a class="+className+" href="+pageHref+">"+navItem[i]+"</a>");    
    }
    
}
function initBanner(){
    $("#banner_text").text("welcome to my ePortfolio!--"+studentName);
    $("#banner_image").attr("src", IMG_PATH+bannerImageFileName);
    
}




function loadData(jsonFile) {
    $.getJSON(jsonFile, function(json) {
	loadEPortfolio(json);
	initPage();
    });
}

function loadEPortfolio(EPortfolioData) {
    title = EPortfolioData.title;
    for (var i = 0; i < EPortfolioData.pages.length; i++) {
	var rawPage = EPortfolioData.pages[i];
        navItem[i]=rawPage.page_title;
        var str=location.pathname;
        a=str.substr(str.lastIndexOf("/")+1);
        if(rawPage.page_title+".html"===a | (i===0 & a==="index.html")){
            currentPage=i;
            pageTitle=rawPage.page_title;
            studentName=rawPage.student_name;
            footer=rawPage.footer;
            bannerImageFileName=rawPage.banner_image_file_name;
            fontFamily=rawPage.font_family;
            fontSize=rawPage.font_size;
            isBold=rawPage.is_bold;
            isItalic=rawPage.is_italic;
            isUnderlined=rawPage.is_underlined;
            layout=rawPage.layout;
            colorTemplate=rawPage.color_template;
            loadHeaderComponents(rawPage.header_components);
            loadListComponents(rawPage.list_components);
            loadParagraphComponents(rawPage.paragraph_components);
            loadImageComponents(rawPage.image_components);
            loadVideoComponents(rawPage.video_components);
            loadSlideshowComponents(rawPage.slideshow_components);
            loadOrder(rawPage.order_components);
        }
    }
}
function loadHeaderComponents(componentsData){
    for (var i = 0; i < componentsData.length; i++) {
        var rawComponent = componentsData[i];
        var component=new HeaderComponent(rawComponent.content);
        headerComponents[i]=component;
    }
    
}
function loadListComponents(componentsData){
    for (var i = 0; i < componentsData.length; i++) {
        var rawComponent = componentsData[i];
        
        var component=new ListComponent(makeList(rawComponent.list),rawComponent.font_family,
                                        rawComponent.font_size, rawComponent.is_bold,
                                        rawComponent.is_italic,rawComponent.is_underlined);
        listComponents[i]=component;
    }
    
}
function loadParagraphComponents(componentsData){
    for (var i = 0; i < componentsData.length; i++) {
        var rawComponent = componentsData[i];
        
        var component=new ParagraphComponent(rawComponent.content,rawComponent.font_family,
                                        rawComponent.font_size, rawComponent.is_bold,
                                        rawComponent.is_italic,rawComponent.is_underlined);
        paragraphComponents[i]=component;
    }
    
}
function loadImageComponents(componentsData){
    for (var i = 0; i < componentsData.length; i++) {
        var rawComponent = componentsData[i];
        var component=new ImageComponent(rawComponent.image_file_name, rawComponent.width, 
                                         rawComponent.height, rawComponent.caption, rawComponent.floating);
        imageComponents[i]=component;
    }
    
}
function loadVideoComponents(componentsData){
    for (var i = 0; i < componentsData.length; i++) {
        var rawComponent = componentsData[i];
        var component=new VideoComponent(rawComponent.image_file_name, rawComponent.width, 
                                         rawComponent.height, rawComponent.caption);
        videoComponents[i]=component;
    }   
}
function loadSlideshowComponents(componentsData){
    for (var i = 0; i < componentsData.length; i++) {
        var rawComponent = componentsData[i];
        
        var component=new SlideshowComponent(rawComponent.title ,makeSlides(rawComponent.slides));
        slideshowComponents[i]=component;
    }
    
}
function loadOrder(componentsData){
    for (var i = 0; i < componentsData.length; i++) {
        var rawComponent = componentsData[i];
        var component=rawComponent.order;
        order[i]=component;
    }
}
function makeSlides(slidesData) {
    var slides=new Array();
    for (var i = 0; i <slidesData.length; i++) {
	var rawSlide = slidesData[i];
	var slide = new Slide(rawSlide.image_file_name, rawSlide.caption);
	slides[i] = slide;
    }
    return slides;
}
function makeList(listData){
    var list=new Array();

    for (var i = 0; i < listData.length; i++) {
        var rawItem = listData[i];
        
        var item=rawItem.content;
        list[i]=item;
    }
    return list;
    

}

var IMG_PATH;
var ICON_PATH;
var IMG_WIDTH;
var SCALED_IMAGE_HEIGHT;
var FADE_TIME;
var SLIDESHOW_SLEEP_TIME;

// DATA FOR CURRENT SLIDE





    







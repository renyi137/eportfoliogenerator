// CONSTANTS
var IMG_PATH;
var ICON_PATH;
var IMG_WIDTH;
var SCALED_IMAGE_HEIGHT;
var FADE_TIME;
var SLIDESHOW_SLEEP_TIME;

// DATA FOR CURRENT SLIDE
var title;
var slides;
var currentSlide;
var slideshowToLoad;
// TIMER FOR PLAYING SLIDESHOW
var timer;



function initSlideshow() {
    IMG_PATH = "./img/";
    ICON_PATH = "./icons/";
    IMG_WIDTH = 1000;
    SCALED_IMAGE_HEIGHT = 500.0;
    FADE_TIME = 1000;
    SLIDESHOW_SLEEP_TIME = 3000;
    slides = new Array();
    var slideshowDataFile = "./data/EPortfolioData.json";
    loadData(slideshowDataFile);
    timer = null;
}

function initPage() {
    $("#slideshow_title").html(title);
    if (currentSlide >= 0) {
	$("#slide_caption").html(slides[currentSlide].caption);
	$("#slide_img").attr("src", IMG_PATH + slides[currentSlide].image_file_name);
	$("#slide_img").one("load", function() {
	    autoScaleImage();
	});
    }
}

function autoScaleImage() {
	var origHeight = $("#slide_img").height();
	var scaleFactor = SCALED_IMAGE_HEIGHT/origHeight;
	var origWidth = $("#slide_img").width();
	var scaledWidth = origWidth * scaleFactor;
	$("#slide_img").height(SCALED_IMAGE_HEIGHT);
	$("#slide_img").width(scaledWidth);
	var left = (IMG_WIDTH-scaledWidth)/2;
	$("#slide_img").css("left", left);
}

function fadeInCurrentSlide() {
    var filePath = IMG_PATH + slides[currentSlide].image_file_name;
    $("#slide_img").fadeOut(FADE_TIME, function(){
	$(this).attr("src", filePath).bind('onreadystatechange load', function(){
	    if (this.complete) {
		$(this).fadeIn(FADE_TIME);
		$("#slide_caption").html(slides[currentSlide].caption);
		autoScaleImage();
	    }
	});
    });     
}

function loadData(jsonFile) {
    $.getJSON(jsonFile, function(json) {
	loadEPortfolio(json);
	initPage();
    });
}

function loadEPortfolio(EPortfolioData) {
    var str=location.pathname;
    var a=str.substr(str.lastIndexOf("/")+1);
    var b=a.substr(0,a.lastIndexOf("."));
    
    var pageIndex=b.substr(0,b.lastIndexOf("_"));
   
    var slideshowIndex=b.substr(b.lastIndexOf("_")+1);
    
    slideshowToLoad=EPortfolioData.pages[pageIndex].slideshow_components[slideshowIndex];
    title=slideshowToLoad.title;
    slides=slideshowToLoad.slides;
    if (slides.length > 0)
	currentSlide = 0;
    else
	currentSlide = -1;
    
}

function processPreviousRequest() {
    currentSlide--;
    if (currentSlide < 0)
	currentSlide = slides.length-1;
    fadeInCurrentSlide();
}

function processPlayPauseRequest() {
    if (timer === null) {
	timer = setInterval(processNextRequest, SLIDESHOW_SLEEP_TIME);
	$("#play_pause_button").attr("src", ICON_PATH + "Pause.png");
    }
    else {
	clearInterval(timer);
	timer = null;
	$("#play_pause_button").attr("src", ICON_PATH + "Play.png");
    }	
}

function processNextRequest() {
    currentSlide++;
    if (currentSlide >= slides.length)
	currentSlide = 0;
    fadeInCurrentSlide();
}